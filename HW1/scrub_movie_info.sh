#!/bin/bash
#this is the starting script for HW #1, it has useful tips, comments and hints embedded in it.  Read it carefully and add your code to answer the questions


#TIP 1: you can pass a arguments (ie the filename to a bash script) using $1, $2, $3 ... 
#example here: https://unix.stackexchange.com/questions/31414/how-can-i-pass-a-command-line-argument-into-a-shell-script
#we want to be able to call our script on different files like this $ ./scrub_movie_info.sh mylist.csv 
INPUTFILE=$1
#from now on we can use the $FILENAME variable for our input file


#TIP 2: recall I said awk is useful for working with columns of data...sometimes when we export data from excel (or other sources) the delimiter (e.g. space, tab, comma) might be different than what awk is expecting. Also, the end of line character could be different than what awk is expecting.  You can change this. 
# examples: https://www.gnu.org/software/gawk/manual/html_node/Print-Examples.html
# so we can let awk know that we are using the comma as a delimiter in our data file.  See what happens, also see what happens when you use $2, $3 etc
#awk -F "," '{print $1}' $INPUTFILE

#TIP 3: recall you can pipe many commands together, what info does the following do (copy and paste it onto the command line)
#cat movie_metadata.csv | grep 'Woody Allen' |  awk -F "," '{print $6}'


#Question 1 to answer: Which three films grossed the largest amount of money? Add the code below and redirect to a file movie_stats.txt 
echo "ANSWER TO QUESTION 1" > movie_stats.txt
#awk -F "," '{print $8 "," $11}' $INPUTFILE | sort -nr | awk -F "," '{print $2}' | head 
awk -F "," '{print $8 "," $11}' $INPUTFILE | sort -nr | awk -F "," '{print $2 " grossed " $1}' | head -n 3 >> movie_stats.txt
#awk -F "," '{a[$1]++} END {for (x in a) {print  a[x] "\t" x}}' $INPUTFILE | sort -nr | head > movie_stats.txt
echo "_______________________________________________________________________________________" >> movie_stats.txt

#Question 2 to answer: What is the average imdb score for all movies directed by Hayao Miyazaki? Add the code below and redirect (append) to the file movie_stats.txt created in Question #1 
echo "ANSWER TO QUESTION 2" >> movie_stats.txt
mean_imdb=$(awk -F "," '$1=="Hayao Miyazaki" {sum += $25; nmovies++} END { print sum/nmovies }' $INPUTFILE)
echo "Average imdb score of movies directed by Hayao Miyazaki is $mean_imdb" >> movie_stats.txt
echo "_______________________________________________________________________________________" >> movie_stats.txt


#Question 3 to answer: How many movies did each director in the data-set direct (hint: we did a very similar task with the tutorial where we analyzd the dictionary words). Add the code to do this below, and redirect thw output to append to movie_stats.txt
#Note: in this solution, the header rows get included in the pipe, and 'director_name' gets counted as a director name.
echo "ANSWER TO QUESTION 3" >> movie_stats.txt
echo "-----------------------------------------------" >> movie_stats.txt
echo -e "Number of Movies\tDirector" >> movie_stats.txt
echo "-----------------------------------------------" >> movie_stats.txt
awk -F "," '{a[$1]++} END {for (x in a) {print  "\t" a[x] "\t\t" x}}' $INPUTFILE | sort -nr >> movie_stats.txt
#The following works to give the same output.
#cut -d , -f 1 $INPUTFILE | sort | uniq -c | sort -nr >> movie_stats.txt
echo "_______________________________________________________________________________________" >> movie_stats.txt


#Don't forget the final 2 tasks (manipulating movie_metadata.csv).  1. Reorder/filter the movie_metatdata.csv such that the movie_title is the first column followed by director_name, title_year, gross, imdb_score, movie_facebook_likes and redirect to a new file movie_metadata_filtered.csv. 2. Add a movie to the end of the list movie_metadata_filtered.csv. 3. Don't forget to add commit and push your changes to your branch
# 1. Filtering movie_metadata.csv
awk -F "," 'BEGIN {FPAT = "([^,]*)|(\"([^\"]|(\"\"))*\")"} {print $11","$1","$23","$8","$25","$26}' $INPUTFILE > movie_metadata_filtered.csv
#awk -F "," '{print $11","$1","$23","$8","$25","$26}' $INPUTFILE > movie_metadata_filtered.csv

# 2. Appending movie 'It'
echo "It,Andy Muschietti,2017,480012164,7.9,1613561" >> movie_metadata_filtered.csv
