# README #

This is homework 4 (I/O and numerical linear algebra) for STC Fall 2017.  This
homework is due on the 5th of December 2017 at 11:59pm.

### Instructions ###

Please complete this assignment on stampede2.tacc.utexas.edu using a command
line editor (vim, emacs, nano) and command line tools.

Please use version control as you do your homework.

Do not hesitate to ask for help and also post to piazza.

This homework guides you through solving a partial differential equation:

```
- d^2 u / dx^2 = f, 0 < x < 1
u(0) = 0
u(1) = 0
```

The derivative will be discretised with central finite differences, resulting
in a linear system of the form `Au = f`.  The matrix A has a very special
structure (addressed in Question 2).

The function f will be provided in an HDF5 file that you will read.  You will
will construct the matrix A.  You will then use an appropriate LAPACK routine to
solve the linear system.

1. The file laplacian.C contains a C++ program.  The function read_rhs is
   responsible for reading in the right-hand side vector from an HDF5 file.
   Implement the appropriate function calls to a) open the dataset; and b)
   read the dataset into the array `rhs`.  Don't forget to clean up the dataset
   and close the file once you're done with it.  You may find the HDF5 tutorial
   and examples on the HDF5 website helpful.  You'll need to update the included
   makefile to compile and link against hdf5.

2. The system, A, has a very special structure:
              |  2 -1             |
              | -1  2 -1          |
   A = h^-2 x |        ...        |
              |          ...      |
              |          -1  2 -1 |
              |             -1  2 |

  It's clear that A is both tridiagonal and symmetric.  It turns out A is also
  positivie definite (all eigenvalues are positive).  The function
  LAPACKE_dpttrf, part of Intel's MKL, is an appropriate function to call to
  factorise tridiagonal symmetric positive definite matrices as `A = LDL^T`
  where D is diagonal and L is unit lower bidiagonal.  The documentation for
  LAPACKE_dpttrf is here:
  https://software.intel.com/en-us/mkl-developer-reference-c-pttrf

  After calling the function, the system is overwritten with the factors L and
  D.  The function LAPACKE_dpttrs can use these factors and the right-hand side
  from Question 1 to solve the system `Au = f` for u.  The documentation for
  LAPACKE_dpttrs is here:
  https://software.intel.com/en-us/mkl-developer-reference-c-pttrs

  Since the boundary conditions for the PDE are zero, we don't need to solve for
  them and so we remove them from the system.  Therefore the size of the system,
  solution, and right-hand side are all two fewer than the number of physical
  gridpoints in the domain [0, 1].  Finish the implementation of the
  `assemble_and_solve` function to assemble the system and make the right calls
  to the LAPACK functions described above.

3. Finally, we write the solution of the system to an HDF5 file.  The rhs
   variable `f` is overwritten with the solution (without the boundary values).
   Finish the implementation of `write_sol` to write the solution vector to an
   HDF5 file.  You may find the HDF5 tutorials and examples helpful.

   You can do `python plot_solution.py` to create a pdf file with a plot of your
   solution.  It should look like this one:
   http://users.ices.utexas.edu/~damon/sol.pdf

Grading criteria:
i.  Frequent use of git.
ii.  Correct code and makefile.

### Need Help? ###

Questions? Don't forget to use our piazza message
board! https://piazza.com/utexas/fall2017/sds335394 and also you can contact UV
Yadav udaivir.yadav@utmail.utexas.edu, Anne Bowen adb@tacc.utexas.edu or Damon
McDougall dmcdougall@tacc.utexas.edu
