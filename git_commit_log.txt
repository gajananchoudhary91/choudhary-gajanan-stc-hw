commit c08c60072df66a6fa148cdf9e483ef9d67e8448b
Author: Gajanan Choudhary <gajanan@utexas.edu>
Date:   Tue Dec 12 19:29:52 2017 -0600

    Added verification test case (as a unit test) to check asymptotic convergence rate. There seems to be a bug in read_dbl_field that needs fixing. It won't read .1, for example. Really need a unit test for parse_card and int-double reading functions. Will write scripts for plotting the solutions next and convergence rates next.

commit 249de260ed0946c09168c7d9936959dd58cfed65
Author: Gajanan Choudhary <gajanan@login3.stampede2.tacc.utexas.edu>
Date:   Tue Dec 12 14:38:42 2017 -0600

    Changes to compile with gcc on stampede. Code coverage using gcov does not work.

commit e8d505312d5acff4809f50fe7e672915ee15a566
Author: Gajanan Choudhary <gajanan@utexas.edu>
Date:   Tue Dec 12 13:14:06 2017 -0600

    Improved unit tests to check for little l2 error, so they now also act as one verification test case. Turned on debug prints. Profiling, coverage, testing, debugging modes all working. Minor warning and bug fixes.

commit 906e3147658a57d6f4e105dda7a4eedbf5c1c720
Author: Gajanan Choudhary <gajanan@utexas.edu>
Date:   Mon Dec 11 22:09:35 2017 -0600

    Added doxygen documentation comments for many, more to be done. Added unit tests. Bug fixes and code rearrangement. Will try to add a convergence test and try to make changes to support a test option through an input file.

commit 6b8f9c3f9a646c9df2b1ddffba2b9ee131e08efd
Author: Gajanan Choudhary <gajanan@utexas.edu>
Date:   Sat Dec 9 16:30:13 2017 -0600

    ODE-project. Lots of additions. This is the first working version on my ICES desktop. Verification pending. Contains forward euler, RK2, RK4, RKF45 time stepping. Using GSL for RK. Added inout folder containing io files, added solver folder containing the time stepping, added a tools folder containing generic functions like mat-vect product, and added the cardmaker program for making new cards. Code verification next.

commit 5e50e206b4f384e58175a442cc30cd7eeaaeb55d
Author: Gajanan Choudhary <gajanan@utexas.edu>
Date:   Tue Dec 5 18:19:23 2017 -0600

    Added changes to compile on stampede2 as well.

commit ac83383d89dd3ab3171c202db92f2bd34c83eede
Author: Gajanan Choudhary <gajanan@utexas.edu>
Date:   Tue Dec 5 17:38:09 2017 -0600

    HW4 - Adding debug infos, decorating the output. Works on ICES local machine after relevant module loads.

commit 6337149b32536a114437c55c151977ac8b666a0c
Author: Gajanan Choudhary <gajanan@utexas.edu>
Date:   Tue Dec 5 16:45:32 2017 -0600

    HW4 Seems to be working. Added an input file for the ode project.

commit 7eede2de34afa8382a6d92c223c6a801c1fb28ab
Merge: 179fb19 eabb681
Author: Gajanan <gajanan@utexas.edu>
Date:   Mon Dec 4 19:38:57 2017 -0600

    Merged annedara/stc-hw into master

commit eabb6811f288bcbb4c33d0079fdeb3e8ba27267e
Merge: 7992253 36c7c28
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Fri Dec 1 16:09:44 2017 -0600

    Merge remote-tracking branch 'adb/master'

commit 7992253e4a40eaa22bbc0a18c2e2f1a5fbeb8b20
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Fri Dec 1 16:09:13 2017 -0600

    Fix type in function parameter

commit 179fb19136cc1d902885fa63b8925f0a7d9c29ce
Merge: 6d137ff 36c7c28
Author: Gajanan <gajanan@utexas.edu>
Date:   Thu Nov 30 14:13:28 2017 -0600

    Merged annedara/stc-hw into master

commit 36c7c2893e69814900d5f52d94f16f24ab307440
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Wed Nov 29 20:43:42 2017 -0600

    Add the right-hand side file
    
    Because I'm an idiot

commit 9b438a761881425e927a956b0ecb0eb9d99088b4
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Tue Nov 28 17:24:49 2017 -0600

    Implement writer template

commit 810f0a16d4dca237b76dee984672e32bed5b66d3
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Tue Nov 28 21:57:58 2017 -0600

    Add Q3

commit 3c2b128506cf5580add8c6d48a95fffcacd6e876
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Tue Nov 28 21:57:36 2017 -0600

    Remove check target

commit 6d137ffe3f8702830463f6a30404bacbafb5d6a7
Merge: 6945af9 3a91d07
Author: Gajanan <gajanan@utexas.edu>
Date:   Tue Nov 28 14:03:20 2017 -0600

    Merged annedara/stc-hw into master

commit 42a9699a10f964924f987576ca09a376c89f2ff4
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Tue Nov 28 08:15:42 2017 -0600

    Add plotter

commit d60c5fe3f4f3317b919fd312df73f8b8696f9eb4
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Tue Nov 28 08:14:35 2017 -0600

    Add Q2

commit bdc1ea7613cdb67726eaf7ee0ff106bead5210a0
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Tue Nov 28 08:12:52 2017 -0600

    Fix up some compiler errors

commit 9a47d9d50de14907052d4faad6058fea6e1a58b1
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Tue Nov 28 07:50:40 2017 -0600

    Adding makefile

commit 5011b74c048628225f1319a2c4933a21d1a8a681
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Tue Nov 28 07:49:55 2017 -0600

    Adding first draft of template HW4 code

commit f233f3d756da5ca91187899bb1c23951810396ff
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Tue Nov 28 07:46:23 2017 -0600

    Make PDE positive definite

commit 1be2895629fa19eeb5ddab6d4422bc1a70d10843
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Mon Nov 27 18:16:39 2017 -0600

    Add Q1 to HW4

commit 891449f207f9da2b2c6fc06ec08305d1a800729e
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Mon Nov 27 17:13:59 2017 -0600

    Clear up some grammar

commit 3a91d07b5a8e8be7d3be1f62d98990812de1a75e
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Mon Nov 27 17:12:40 2017 -0600

    Adding initial README for HW4

commit 6945af9c9940e845e4a91f1618909dd33be88669
Author: Gajanan <gajanan@utexas.edu>
Date:   Mon Nov 27 06:05:40 2017 +0000

    README.md edited online with Bitbucket

commit 655351e7182540baed88671442c4abccd92614fa
Author: Gajanan <gajanan@utexas.edu>
Date:   Mon Nov 27 06:02:56 2017 +0000

    README.md edited online with Bitbucket

commit 17b05ee595fcaa73c95560dc8d40a8c9bc54ad97
Author: Gajanan <gajanan@utexas.edu>
Date:   Mon Nov 27 06:00:51 2017 +0000

    README.md edited online with Bitbucket

commit f2a8039c66563a7f2874a91b7e380ce0bc3f3c9d
Author: Gajanan Choudhary <gajanan@utexas.edu>
Date:   Sun Nov 26 23:58:21 2017 -0600

    First commit of ODE project. We will use C and CMake for this project. This commit contains a boilerplate for the program that we will add to and build upon.

commit 92d22680968fee3f492426422d48f59111c612e1
Merge: 35312ee 0af5085
Author: Gajanan <gajanan@utexas.edu>
Date:   Sun Nov 26 19:32:36 2017 -0600

    Merged annedara/stc-hw into master

commit 0af5085fd2157b495c296f4dd6803e7eac530188
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Fri Nov 24 11:36:57 2017 -0600

    Add more missing lecture notes

commit 1679788d57549cf9a0f2fbfb51c08d7d5fad9517
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Fri Nov 24 11:35:00 2017 -0600

    Add missing lecutre notes

commit 4ed45fb82ca9d37a87d1ac53969466e737579c5c
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Fri Nov 24 11:32:14 2017 -0600

    Add more lecture notes

commit 35312ee0f5c1cdbb4f37b615baf6f1575c28bb7c
Author: Gajanan Choudhary <gajanan@utexas.edu>
Date:   Fri Oct 27 19:02:36 2017 -0500

    Adding answer to question 5.

commit fd46d3bb5ad63c621fd0500bdc0d00ba295fbbe9
Author: Gajanan Choudhary <gajanan@utexas.edu>
Date:   Fri Oct 27 18:57:37 2017 -0500

    Adding answer to question 4.

commit d192624249d11bf8e5198a98ab574e66a588d38c
Author: Gajanan Choudhary <gajanan@utexas.edu>
Date:   Fri Oct 27 17:01:51 2017 -0500

    Adding answer to question 3.

commit ff97d80035163f75b2a93ac5c4f3d13b5217fbc5
Author: Gajanan Choudhary <gajanan@utexas.edu>
Date:   Fri Oct 27 16:58:11 2017 -0500

    Added gprof compiler flag and profiled the integration routine.

commit 2f33dffeb2cad4fdf2a99035ae1873c98e1099e4
Author: Gajanan Choudhary <gajanan@utexas.edu>
Date:   Fri Oct 27 16:54:35 2017 -0500

    Added the gsl integrating function. Works on the ICES desktop at least.

commit 1f95627932c85553d6e12fdd1a93a1dc6c58e75d
Merge: 2e28dd0 998346f
Author: Gajanan <gajanan@utexas.edu>
Date:   Thu Oct 26 21:31:42 2017 -0500

    Merged annedara/stc-hw into master

commit 998346f2cfedbde4d885d22c369553b8b12ebe75
Author: Anne Bowen <adb@login1.stampede2.tacc.utexas.edu>
Date:   Thu Oct 26 12:40:26 2017 -0500

    cleaned up some filenames and added missing lecture notes

commit 6847b25f30aaeae467243f1335b6a11969480212
Author: Anne Bowen <adb@login3.stampede2.tacc.utexas.edu>
Date:   Thu Oct 26 11:58:29 2017 -0500

    added lecture notes

commit dc82bb14e1f432337e26ee1d3bd6721823be1035
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Wed Oct 25 17:20:31 2017 -0500

    Add grading criteria

commit e0750ca800bc0691586d74d56cd90e3f9126d882
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Wed Oct 25 17:15:49 2017 -0500

    Rename integrate.C to integration.C and update info

commit 7f029e47bb8d0699b43f244c072bb7bb845c5b57
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Wed Oct 25 17:13:30 2017 -0500

    Add integrate to ignores

commit 42a058cd703211a4c16efabbdfa7f3d4d2b6324d
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Wed Oct 25 17:01:24 2017 -0500

    Add hw3.txt

commit bc79df3dc58b73c0e4fac09eecde29b257eaf429
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Wed Oct 25 17:00:26 2017 -0500

    Ask students to commit Q3 answer

commit 435118fecd3e189b8128d1bf027fd5bd360fe44a
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Wed Oct 25 17:00:01 2017 -0500

    Add Q5

commit bb4700c227926e2b125ac174d8aa8bab92f12319
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Wed Oct 25 16:59:56 2017 -0500

    Add Q4

commit b4896c0cdb454cb381efaa298d9955b90d474a7a
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Wed Oct 25 16:59:42 2017 -0500

    Reflow Q3

commit 8e5204e75daf34e9dac72b6af0f564dba2e3ccd0
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Wed Oct 25 16:59:03 2017 -0500

    Update Q2 instructions for GSL params

commit 540d6ba18ecd62d68d0415a5d0e96a3234cea2ec
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Wed Oct 25 16:36:54 2017 -0500

    Fix compiler warnings

commit bd94c40e2b588b27e7afaf346c7d1c0d4fb49f55
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Wed Oct 25 16:34:34 2017 -0500

    Make the f function GSL-friendly :)

commit 304ea10a00013be987b8631878c85b0cf9e50850
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Wed Oct 25 16:34:21 2017 -0500

    Add instructions for question 3

commit 2152a0908799ebc583f7eb998b3c68151c6e701e
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Wed Oct 25 16:34:07 2017 -0500

    Add instructions for question 2

commit 2ca6bca4415c566b1f60521d8b0069c5db0edce6
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Wed Oct 25 16:33:30 2017 -0500

    Adding test for integrate2

commit 7e778dd5c90cebf5b642884aa453378140435e0f
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Wed Oct 25 16:30:59 2017 -0500

    Add placeholder make variables for flags

commit e234d318ce550d6b062e5aa3cdbfa27e3d902ba1
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Wed Oct 25 14:49:49 2017 -0500

    Factor out testing code into a function

commit 0080666594c75fb6448eb981813c221f1ce47de7
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Wed Oct 25 14:08:12 2017 -0500

    Fix typo

commit 489936954114c3301ba06978b8563af4b09ab9c3
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Tue Oct 24 17:59:48 2017 -0500

    Adding README.md with first question

commit 0c54c723ff2db23f662eb0eed1e17956de5b9712
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Tue Oct 24 17:32:23 2017 -0500

    Adding a Makefile

commit febc29fa160948a57a680e4e7fcdd7307aac6cd8
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Tue Oct 24 14:21:25 2017 -0500

    Printing PASS/FAIL feedback on integrate() function

commit bfea795364df1f0560179da4ba64d6e9a7f93a72
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Tue Oct 24 14:20:43 2017 -0500

    Adding placeholder GSL quadrature function

commit 5b77efffa0540d4f4a3a31b37ee4d8d4a6f5efa4
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Tue Oct 24 11:14:48 2017 -0500

    Adding dumb quadrature implementation

commit 2e28dd0507d280cc73f11661e26e4aba9d1a5d1c
Author: Gajanan Choudhary <gajanan@utexas.edu>
Date:   Sat Oct 14 23:19:12 2017 -0500

    Adding answer to question 9. Modified code to implement a better way of calculating roots. Output file is now written roots_updated.txt instead of earlier roots.txt.

commit 6a630d3370a6fd377f728346a3eef43709ba0618
Author: Gajanan Choudhary <gajanan@utexas.edu>
Date:   Sat Oct 14 22:39:09 2017 -0500

    Adding answer to question 8.

commit b1fec02df10308924d18d44ffc2af414eb4da204
Author: Gajanan Choudhary <gajanan@utexas.edu>
Date:   Sat Oct 14 22:38:30 2017 -0500

    Adding answer to question 7.

commit 73624b60d3274bd87aa141269928392650d4153c
Author: Gajanan Choudhary <gajanan@utexas.edu>
Date:   Sat Oct 14 18:05:04 2017 -0500

    Adding answer to question 6.

commit 7606f362aecd97fec141624ff39352c124f82a15
Merge: c2727bf 65319f5
Author: Gajanan <gajanan@utexas.edu>
Date:   Sat Oct 14 17:31:39 2017 -0500

    Merged annedara/stc-hw into master

commit c2727bf902b04afcbc834b61497b9ec4127c563e
Author: Gajanan Choudhary <gajanan@utexas.edu>
Date:   Sat Oct 14 17:31:04 2017 -0500

    Adding answers to questions 4 and 5.

commit ddf0c7c71d9d2468b892825247c30b1bcd5d5ab0
Author: Gajanan Choudhary <gajanan@utexas.edu>
Date:   Wed Oct 11 23:17:28 2017 -0500

    Adding answer to question 2. Adding comments on how to use the code.

commit 34b4d462fdad785c36a261c5e62794d95d0cbca4
Author: Gajanan Choudhary <gajanan@utexas.edu>
Date:   Wed Oct 11 22:21:24 2017 -0500

    Adding answer to question 1 - writing a code to find roots of a quadratic.

commit 65319f5f852e13e3639487fe5d18c6649ae78ed6
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Wed Oct 11 21:06:57 2017 -0500

    Add due date/time

commit 1695e348a38793129dc7969980d9d56a84ba6891
Merge: 863eba9 0d4721d
Author: Gajanan Choudhary <gajanan@utexas.edu>
Date:   Tue Oct 10 14:41:01 2017 -0500

    Merging with anne's repo after a git pull. Not sure I am doing this correctly.

commit 0d4721d5725dfd999af6f19dc6e55657870f2314
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Mon Oct 9 14:39:50 2017 -0500

    Add the word 'discuss' to Q8

commit 6148e94491c2b357504ffcd330edd4bc83d40d37
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Mon Oct 9 14:39:38 2017 -0500

    Tell students to commit fixed roots file

commit fbae022489493d8d4a0a481550e71bce109691d4
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Mon Oct 9 14:39:10 2017 -0500

    Add grading criteria

commit cc2d3882ffbffdfbc1c002150bcab7665c5c9b91
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Mon Oct 9 14:13:19 2017 -0500

    Tell students to commit latexified answers

commit c7dd032809c906e57dead59b704235be9da18ac6
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Mon Oct 9 14:11:45 2017 -0500

    Make it clear students must commit source

commit 415995cd66ada040fe3a7d0d0438aae5f70919df
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Mon Oct 9 14:09:34 2017 -0500

    Adding tex instructions and source, update ignores

commit bc5519091f67ef66308e20d2446bd324b303816b
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Mon Oct 9 14:03:14 2017 -0500

    Fix typo in question 8

commit 863eba9498d112346aa3e9638a2a086e8a3f741d
Author: Gajanan Choudhary <gajanan@utexas.edu>
Date:   Sun Oct 8 16:46:47 2017 -0500

    Minor changes to match with Anne's repo.

commit 30068b75898d22e4c6b36ac446da0ac693c94361
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Sat Oct 7 22:53:20 2017 -0500

    Add Q7, Q8 and Q9

commit 4a0edc63f217ee3afedd69a265330614db3107e2
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Sat Oct 7 22:52:41 2017 -0500

    Update the setup question to label the solutions
    
    And make Q6 refer to a specific solution.  This is so I don't have to
    guess how the students labelled the roots.

commit d8e3d8dc356f72d6acd2bb397865b8a8f4ef289e
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Sat Oct 7 22:52:13 2017 -0500

    Making Q5 another hand-calculation

commit dc6fc3718a7055db2e6adfbf7013232542133c88
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Sat Oct 7 22:04:37 2017 -0500

    Tell students to commit the roots file to their repos

commit 6973ebaab68853a45884b176d8e003a91a7d14f4
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Sat Oct 7 22:04:21 2017 -0500

    Fix typos in instructions

commit 92ab4e1ffcb62500bc03cd337c85565980ef7407
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Sat Oct 7 21:58:15 2017 -0500

    Add Q5, observation about one of the roots

commit 34c23b19e06c2be4d6d52afa8c928773b3913070
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Sat Oct 7 21:56:02 2017 -0500

    Add Q4, a short 'proof'

commit 9f8d70333b53b743741e191a19e5cce77357c504
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Sat Oct 7 21:55:16 2017 -0500

    Add Q3
    
    Q3 is to explore roots as b increases

commit 466c0a66f517ffdf9c7b5f56f64ea5ee48f83615
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Sat Oct 7 21:54:54 2017 -0500

    Clarify a, b, and c are doubles

commit 90d0a60ada56a26333ac500e3f161c33ea488eca
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Sat Oct 7 21:54:24 2017 -0500

    Add C++ file type for Q1

commit 56d874d21511dc583f720d6aa8c37d3782770e84
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Sat Oct 7 21:21:00 2017 -0500

    Add some software design advice for Q2

commit 7362e613568be4d0fe76cd849096a68eb8d89669
Author: Damon McDougall <damon.mcdougall@gmail.com>
Date:   Sat Oct 7 21:15:38 2017 -0500

    Starting instructions for HW2

commit 46eb0906faf432ed01749a5073542746241ceb78
Author: Gajanan Choudhary <gajanan@utexas.edu>
Date:   Thu Sep 28 19:47:30 2017 -0500

    Updating git log file

commit 8c963a5bf57175234fde2d9ac1c717360bc34ae9
Author: Gajanan Choudhary <gajanan@utexas.edu>
Date:   Thu Sep 28 19:46:34 2017 -0500

    Added changes so that movie names containing commas are properly dealt with by awk as part of the names, instead of reading such commas as a delimiter.

commit da2e0991f47531b165dc66f2de6fe5c0abf3b6c7
Author: Anne Bowen <adb@tacc.utexas.edu>
Date:   Thu Sep 28 18:22:05 2017 +0000

    scrub_movie_info.sh edited online with Bitbucket

commit 01d3603afbb258c8f4f71da16b04c81f63ee6594
Author: Anne Bowen <adb@tacc.utexas.edu>
Date:   Thu Sep 28 18:20:31 2017 +0000

    README_HW1.md edited online with Bitbucket

commit 4f565161b13e34256b81e84cb49bbef9f245b930
Author: Anne Bowen <adb@tacc.utexas.edu>
Date:   Thu Sep 28 17:41:46 2017 +0000

    fixed typo movie_stat.txt changed to movie_stats.txt

commit e1d92b423dcc01c8e8709879663f9de988c1fe30
Author: Gajanan Choudhary <gajanan@utexas.edu>
Date:   Wed Sep 27 16:45:24 2017 -0500

    Adding Answers to HW1, task 2. Added the git log file in the root directory

commit 09f4b749dc1735117d34da2401e0f8d18e5476d8
Author: Gajanan Choudhary <gajanan@utexas.edu>
Date:   Wed Sep 27 16:36:34 2017 -0500

    Committing incrementally - Added answers to questions 1, 2, and 3 in HW1.

commit d46f67e8d8da62dd8bbe4574302457ed7049c9bb
Author: Anne Dara Bowen <adb@tacc.utexas.edu>
Date:   Mon Sep 25 12:36:29 2017 -0500

    finished editing the HW1 README

commit 62362e5402627c3c6b47f208c35df62ecc1fcb6c
Author: Anne Dara Bowen <adb@tacc.utexas.edu>
Date:   Mon Sep 25 11:40:41 2017 -0500

    updated README to be specific to HW1

commit 9eb25f2192eac368b78322bab7ddbf5cf5a07a3c
Author: Anne Dara Bowen <adb@tacc.utexas.edu>
Date:   Mon Sep 25 10:29:24 2017 -0500

    restructed HW1

commit 4089976da20e3065ddc35f736bb9b03d70c17841
Author: Anne Bowen <adb@tacc.utexas.edu>
Date:   Mon Sep 25 15:15:55 2017 +0000

    changing HW README to be general for all HW

commit bcef147abecf8449b8f14a925ddb967aa2d79335
Author: Anne Bowen <adb@tacc.utexas.edu>
Date:   Thu Sep 21 15:13:33 2017 +0000

    README.md edited for clarity

commit 59cdd12a43985642cfaaa9b2e7ae17bb87910a40
Merge: 06fb13a 40fc63d
Author: Anne Dara Bowen <adb@tacc.utexas.edu>
Date:   Thu Sep 21 10:11:07 2017 -0500

    fixed README merge conflict

commit 06fb13a048f3eba66018d73dfb3195250acef647
Author: Anne Dara Bowen <adb@tacc.utexas.edu>
Date:   Thu Sep 21 10:07:37 2017 -0500

    added some file manipulation tasks

commit 40fc63dcfa5c40e5fb6b99e540a44f434c35082c
Author: Anne Bowen <adb@tacc.utexas.edu>
Date:   Thu Sep 21 14:55:11 2017 +0000

    README.md added hint for pushing to branch

commit c82769aa1772c5a5c0b3dd0b1ef8f133d36e2135
Merge: 8b0ff7b b94eaa3
Author: Anne Dara Bowen <adb@tacc.utexas.edu>
Date:   Thu Sep 21 09:47:19 2017 -0500

    sync with online edits of README
    Merge branch 'master' of ssh://bitbucket.org/annedara/stc-hw1

commit 8b0ff7b11bf68084d4ea9275316244dc3f20bd65
Author: Anne Dara Bowen <adb@tacc.utexas.edu>
Date:   Thu Sep 21 09:46:55 2017 -0500

    fixed questions to match README

commit b94eaa30cde81f2aedecbca0a2651eb602f6a8cf
Author: Anne Bowen <adb@tacc.utexas.edu>
Date:   Thu Sep 21 14:46:25 2017 +0000

    README.md edited online to further fix formatting and remove merge question

commit 2062af5dbd0430e9bd2ae14e4978006f4570ace2
Author: Anne Bowen <adb@tacc.utexas.edu>
Date:   Thu Sep 21 14:43:25 2017 +0000

    README.md fixed formatting of questions

commit 135b9b8614e9847625411d69c875095e7884c5c1
Author: Anne Bowen <adb@tacc.utexas.edu>
Date:   Thu Sep 21 14:42:25 2017 +0000

    README.md edited questions

commit 9b70819a5f99341081434d12882e10ee7afccaf2
Author: Anne Dara Bowen <adb@tacc.utexas.edu>
Date:   Thu Sep 21 09:37:56 2017 -0500

    added 3 questions

commit 9bfd6e1d96b8bc8eed5e6dbb966aba436a2b68fb
Author: Anne Dara Bowen <adb@tacc.utexas.edu>
Date:   Wed Sep 20 16:54:11 2017 -0500

    updated the sample scripts with more details and prompts for Questions 1 and 2

commit 4498e2f946287a20c14119b269acc771f6838c0d
Author: Anne Bowen <adb@tacc.utexas.edu>
Date:   Wed Sep 20 21:53:00 2017 +0000

    README.md alpha

commit ab266f2f03f4dcd5c5f8ba075bfb7f2eedaa7804
Merge: 470d75b adb1c7a
Author: Anne Dara Bowen <adb@tacc.utexas.edu>
Date:   Wed Sep 20 16:14:56 2017 -0500

    merge in order to sync changes to README I made from bitbucket webpage
    Merge branch 'master' of ssh://bitbucket.org/annedara/stc-hw1

commit 470d75bd43ef46c42f776718a0f6b5abeb3dae08
Author: Anne Dara Bowen <adb@tacc.utexas.edu>
Date:   Wed Sep 20 16:14:27 2017 -0500

    added sample shell script and questions

commit adb1c7a7034655ef3724fe51c0d2bc5b6f8308af
Author: Anne Bowen <adb@tacc.utexas.edu>
Date:   Wed Sep 20 19:45:45 2017 +0000

    README.md edited with purpose of homework and references

commit 6fe88730a905e6d0f950c96d0e6de846457464d7
Merge: e839691 a047591
Author: Anne Bowen <adb@tacc.utexas.edu>
Date:   Wed Sep 20 19:13:26 2017 +0000

    Merged in annedara/placeholder-readmemd-for-hw1-created-onl-1505933837507 (pull request #1)
    
    placeholder README.md for HW1 created online with Bitbucket
    
    Approved-by: Anne Bowen <adb@tacc.utexas.edu>

commit a0475917adb6e7f287b915d361f4627c2665b081
Author: Anne Bowen <adb@tacc.utexas.edu>
Date:   Wed Sep 20 18:57:23 2017 +0000

    placeholder README.md for HW1 created online with Bitbucket

commit e839691807c10350c17defcd7cb8d3b7941e46f0
Author: adb <adb@isp02.tacc.utexas.edu>
Date:   Wed Sep 20 13:36:20 2017 -0500

    sample data-file
