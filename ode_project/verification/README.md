# README #

### INSTRUCTIONS ###
Given in this folder are a number of sample input files. There are 2 problems 
solved by each of forward Euler, RK2, RK4, and RKF45 methods. To run any of 
the problems, first compile the project to get the executable/binary `ode`.
Copy the files so that the binary and the input files are in the same folder,
and run the following on the command line:

    ./ode problem1_FEULER

or

    ./ode problem2_RKF45

The first one runs problem1_FEULER.dat as input, whereas the second one runs
with problem2_RKF45.dat as input.

Note: All input files MUEST end with the extension `*.dat`

### Need Help? ###
Feel free to point out issues or ask questions by mailing me at:
gajanan<AT>utexas.edu.
