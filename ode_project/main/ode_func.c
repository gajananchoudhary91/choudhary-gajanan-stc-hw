/*! \file ode_func.c
 * \brief Contains the initialize, run, and finalize functions of the project.
 * \details
 * */
#include "global_header.h"

static int DEBUG = ON;

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/*! \fn int ode_initialize(MODEL_STRUCT **, char *)
 * \brief Initialize function calling input parser to set up the model.
 * \param model_ptr  Pointer to the address of the MODEL_STRUCT pointer.
 * \param model_name Name of the input file (except for the *.dat part).
 * \return SUCCESS   If initialization was successful.
 * */

int ode_initialize(MODEL_STRUCT **model_ptr, char *model_name) {
    int ierr=SUCCESS;
    MODEL_STRUCT *mod = NULL;

    PRINT_FEW_STARS();
    printf("ODE Main Initializing\n");

#ifdef D_DEBUG
    if (OFF != DEBUG) {
        printf("Reading input file \"%s.dat\".\n", model_name);
    }
#endif

    ierr = read_main(model_ptr, model_name);
    mod = *model_ptr;

    if  (  CARD_RK2   == mod->flag.SOLVER_TYPE
        || CARD_RK4   == mod->flag.SOLVER_TYPE
        || CARD_RKF45 == mod->flag.SOLVER_TYPE ){
#ifdef D_GSL
        ierr = gsl_alloc_init(*model_ptr);
        if (SUCCESS != ierr) {
             printf("Error setting up the GSL system and driver.\n");
             return FAILURE;
        }
#else
        model_free(model_ptr);
        printf("Error. Code MUST be compiled with GSL to use RK methods.\n");
        return FAILURE;
#endif
    }

    /* Open output file for writing. */
    write_initial_output(mod->t_prev, mod->sol_old, mod->nvar, model_name);

    return ierr;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/*! \fn int ode_run(MODEL_STRUCT *) {
 * \brief Finalizing function calling functions to clean up the simulation.
 * \param mod        Pointer to the model.
 * \return SUCCESS   If model simulation was successful.
 * */
int ode_run(MODEL_STRUCT *model) {
    int ierr=SUCCESS;

    PRINT_FEW_STARS();
    printf("ODE Main Running\n");
#ifdef D_DEBUG
    if(DEBUG){
        printf("**************\n");
        printf("Starting at time t_prev = %15.6e\n", model->t_prev);
    }
#endif

    model->t_curr = model->t_prev + model->dt;
    do {
#ifdef D_DEBUG
        if(DEBUG){
            printf("**************\n");
            printf("Solving for time t_curr = %15.6e\n", model->t_curr);
        }
#endif

        /* Solve the time step */
        ierr = solve_main(model);
        if (SUCCESS != ierr) return ierr;

        /* Write output */
        write_main(model->t_curr, model->sol, model->nvar, model->model_name);
        
    }while (model_check_update_time(model)!=YES);

    return ierr;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/*! \fn int ode_finalize(MODEL_STRUCT **) {
 * \brief Finalizing function calling functions to clean up the simulation.
 * \param model_ptr  Pointer to the address of the MODEL_STRUCT pointer.
 * \return SUCCESS   If finalize was successful.
 * */
int ode_finalize(MODEL_STRUCT **model_ptr) {
    int ierr=SUCCESS;

    PRINT_FEW_STARS();
    printf("ODE Main Finalizing\n");

    ierr = model_free(model_ptr);
    return ierr;
}
