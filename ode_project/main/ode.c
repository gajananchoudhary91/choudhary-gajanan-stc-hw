/*!
 * \mainpage Main page
 * \section intro_sec Introduction
 *
 * This code solves first order ordinary differential equations (ODEs) of the
 * type
 *
 * \f[
 * {\frac{du(t)}{dt}} = [K]{u(t)}, t>t_0
 * \f]
 * \f[
 *           {u(t_0)} = {u_0}
 * \f]
 *
 * where 
 * \f$t\f$ is an independent variable (e.g. time),
 *
 * \f$u\f$ is the solution variable, a function of \f$t\f$,
 *
 * \f${u_0}\f$ is the vector of initial conditions at \f$t=t_0\f$
 *
 * \f$[K]\f$ is a matrix of constants.
 *
 * The code allows 4 time stepping options:
 *   -# Forward Euler/explicit time stepping,
 *   -# 2-step Runge-Kutta (RK2) time stepping,
 *   -# Classical 4-step Runge-Kutta (RK4) time stepping, and
 *   -# Runge-Kutta-Fehlberg (4, 5)-step time stepping.
 *
 * The Runge-Kutta time stepping options are implemented through the GNU 
 * scientific library.
 *
 * */


/****************************************************************************/
#include "global_header.h"

static time_t time0, time1, time2;/*! For total simulation time calculation */

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/*!
 * \file    ode.c
 * \author  Gajanan Choudhary (gajanan@utexas.edu)
 * \date    12/09/2017
 * \version 1.0
 * 
 * \brief Main program that initializes, runs, and finalizes the simulation.
 *
 * \details The main program accepts command line arguments, checks that the 
 * number of arguments is exactly 1, viz. the input file. This function then 
 * calls the initialize, run, and finalize functions, prints the timing 
 * information and checks if the run was successful or not. The 
 * program initializes the model by reading the input files and allocating 
 * the memory. It then runs the simulation by marching through the time 
 * steps, and finalizes the program by freeing the memory. Timing 
 * information is printed, and the program exits after checking whether the 
 * run was a success or a failure.
 * */

/*!
 * \param argc is the number of input arguments.
 * \param argv is an array of the input arguments.
 * \return integer EXIT_SUCCESS for a succesful run, EXIT_FAILURE for a 
 * failed run.
 * */
int main(int argc, char *argv[]){
    int ierr=0;    /* Error tracking variables. */
    double initialize_time=0.0; /* Timing ode_initialize() function call. */
    double run_time=0.0;        /* Timing ode_run() function call. */
    double finalize_time=0.0;   /* Timing ode_finalize() function call. */
    double total_time=0.0;      /* Total run time of the code. */
    MODEL_STRUCT *model=NULL;   /* The model data structure declaration. */
    
    /* Preliminary checks for number of input arguments */
    time(&time0);
    if (argc < 2) {
        printf("\nError: No command line arguments supplied!\n");
        printf("Please enter the name of the input file as a " 
               "command line argument...\n");
        printf("Terminating program run...\n");
        exit(EXIT_FAILURE);
    }
    else if (argc > 2) {
        printf("\nError: Too many command line arguments supplied!\n");
        printf("Please enter only the name of the input file as a "
               "command line argument...\n");
        printf("Terminating program run...\n");
        exit(EXIT_FAILURE);
    }
    else {
        printf("\n\n"); PRINT_MANY_STARS();
        printf("Starting program run\n");
        PRINT_MANY_STARS();
    }

    time(&time1);
    /*************************************/
    /* Allocate memory, initialize model, read input, and set up the run. */
    ierr = ode_initialize(&model, argv[1]);
    /*************************************/
    CHECKSUCCESS(ierr);
    time(&time2);
    initialize_time = difftime(time2, time1);

    /*************************************/
    /* Run the model simulation. */
    ierr = ode_run(model);
    /*************************************/
    CHECKSUCCESS(ierr);
    time(&time1);
    run_time = difftime(time1, time2);

    /*************************************/
    /* Clean up after a successful run. */
    ierr = ode_finalize(&model);
    /*************************************/
    CHECKSUCCESS(ierr);
    time(&time2);
    finalize_time = difftime(time2,time1);

    total_time = difftime(time2,time0);

    /* Print timing information. */
    PRINT_FEW_STARS();
    printf("    Initialize time = % .6fs\n", initialize_time);
    printf("    Run time        = % .6fs\n", run_time);
    printf("    Finalize time   = % .6fs\n", finalize_time);
    printf("    Total time      = % .6fs\n", total_time);
    PRINT_MANY_STARS();

    printf(COLOR_GREEN "Program exited with SUCCESS" COLOR_RESET "\n");
    PRINT_MANY_STARS(); printf("\n\n");
    return EXIT_SUCCESS;
}
