#include "global_header.h"
#define MAX_INT_LENGTH  9

static int DEBUG = OFF;

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
void remove_spaces(char **outstr){
    char *i = *outstr;
    while ((' ' == *i || '\t' == *i) && (0 != *i) && ('\n' != *i)){
        i++;
    }
    if (0 != *i /*&& '\n' != *i Commenting due to conflict with parse_card*/){
        *outstr = i;
    }
    else{
        *outstr = NULL;
    }
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
int read_int_field(char **outstr, int *status){
    int outint = NO;
    int numlength = 0;
    char *i = *outstr;
    remove_spaces(&i);
    *outstr = i;
    if ('-'==*i || '+'==*i) {
        i++;
    }
    if ((!*i) || 0 == *i || '\n' == *i || ' ' == *i || '\t' == *i){
#ifdef D_DEBUG
        if (DEBUG) printf("\nError: No number encountered in the string");
        else throw_error("\nError: No number encountered in the string");
#else
        throw_error("\nError: No number encountered in the string");
#endif
        *status = READ_FAILURE;
        return NO;
    }
    while (' '!=*i && '\t'!=*i && '\n'!=*i && 0!=*i){
        if ('0'<=*i && '9'>=*i){
            numlength++;
        }
        else{
#ifdef D_DEBUG
            if(DEBUG) printf("\nError: Encountered a non-numeric "
                             "character while reading the integer.");
            else throw_error("\nError: Encountered a non-numeric "
                             "character while reading the integer.");
#else
            throw_error("\nError: Encountered a non-numeric "
                        "character while reading the integer.");
#endif
            *outstr = i;
            *status = READ_FAILURE;
            return NO;
        }
        i++;
    }
    if (numlength > MAX_INT_LENGTH){
#ifdef D_DEBUG
        if (DEBUG) printf("\nError: Integer too long. Only "
                          "upto %i digits allowed.", MAX_INT_LENGTH);
        else throw_error("\nError: Integer too long.");
#else
        throw_error("\nError: Integer too long.");
#endif
        *outstr = i;
        *status = READ_FAILURE;
        return NO;
    }

    /* If we're here, we can finally read the string! */
    outint = (int) strtol(*outstr, &i, 10);
#ifdef D_DEBUG
    if (DEBUG) printf("\n outstr : %s      i : %s", *outstr, i);
#endif
    *outstr = i;
    *status = READ_SUCCESS;
    return outint;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
double read_dbl_field(char **outstr, int *status){
    double outdouble = NO;
    int pcount = 0, ecount = 0;
    int numlength = 0;
    char *i = *outstr;
    remove_spaces(&i);
    *outstr = i;
    if ('-' == *i || '+' == *i) {
        i++;
    }
    if ((!*i) || 0 == *i || '\n' == *i || ' ' == *i || '\t' == *i){
#ifdef D_DEBUG
        if (DEBUG) printf("\nError: No number encountered in the string");
        else throw_error("\nError: No number encountered in the string");
#else
        throw_error("\nError: No number encountered in the string");
#endif
        *status = READ_FAILURE;
        return NO;
    }
    while (' ' != *i && '\t' != *i && '\n' != *i && 0 != *i){
        if ('0' <= *i && '9' >= *i){
            numlength++;
        }
        else if (1 > pcount && '.' == *i && 0 < numlength){
            pcount++;
        }
        else if (1 > ecount && ('e' == *i || 'E' == *i) && 0 < numlength){
            ecount++;
        }
        else if (  ('E' == (*(i-1)) || 'e' == (*(i-1))) 
                && ('-' == *i       || '+' == *i      ) ){
            /* Do nothing, this one's allowed. */
        }
        else{
#ifdef D_DEBUG
            printf("\n%s", i);
            if(DEBUG) printf("\nError: Encountered a non-numeric "
                             "character while reading the double.");
            else throw_error("\nError: Encountered a non-numeric "
                             "character while reading the double.");
#else
            throw_error("\nError: Encountered a non-numeric "
                        "character while reading the double.");
#endif
            *outstr = i;
            *status = READ_FAILURE;
            return NO;
        }
        i++;
    }
    if (0 == numlength){
#ifdef D_DEBUG
        if (DEBUG) printf("\nError: No number encountered in the string");
        else throw_error("\nError: No number encountered in the string");
#else
        throw_error("\nError: No number encountered in the string");
#endif
        *status = READ_FAILURE;
        return NO;
    }
    
    /* If we're here, we can finally read the string! */
    outdouble = (double) strtod(*outstr, &i);
#ifdef D_DEBUG
    if (DEBUG) printf("\n outstr : %s      i : %s", *outstr, i);
#endif
    *outstr = i;
    *status = READ_SUCCESS;
    return outdouble;
}

