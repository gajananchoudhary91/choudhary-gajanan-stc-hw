#include "global_header.h"

static int DEBUG = ON;
static int DEEP_DEBUG = OFF;
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
void read_TIME(MODEL_STRUCT *mod, char *data){
    int status = 0;          /* Keeping track of read status*/
    double tempdouble = 0.0; /* Temporary double */
    CARD card;               /* For input card detection */

#if D_DEBUG
    if (DEEP_DEBUG){
        printf("Inside read_TIME with data=%s\n", data);
    }
#endif
    card = parse_card(&data);
    switch(card){
        /**********************/
        case CARD_DT:
            card = parse_card(&data);
            if (CARD_FIXED == card){
                mod->flag.ADAPT_TIME = NO;
#ifdef D_DEBUG
            if (DEBUG) printf("Using fixed time step throughout.\n");
#endif
            }
            else if (CARD_VAR == card) {
                mod->flag.ADAPT_TIME = YES;
#ifdef D_DEBUG
            if (DEBUG) printf("Using adaptive time stepping.\n");
#endif
            }
            else{
                throw_error("Unrecognized card after card PROB.");
            }
            tempdouble = read_dbl_field(&data, &status);
            if (READ_SUCCESS == status) mod->dt = tempdouble;
            else throw_error("Error reading time step");
#ifdef D_DEBUG
            if (DEBUG) printf("Initial time step = % 15.6e\n", mod->dt);
#endif
            mod->max_dt = mod->dt;
            break;
        /**********************/
	case CARD_TSTART:
	    tempdouble = read_dbl_field(&data, &status);
	    if (READ_SUCCESS == status) mod->tStart = tempdouble;
	    else throw_error("Error reading start time");
#ifdef D_DEBUG
	    if (DEBUG) printf("Starting time = % 15.6e\n", mod->tStart);
#endif
	    break;
        /**********************/
        case CARD_TEND:
            tempdouble = read_dbl_field(&data, &status);
            if (READ_SUCCESS == status) mod->tEnd = tempdouble;
            else throw_error("Error reading end time");
#ifdef D_DEBUG
            if (DEBUG) printf("Ending time   = % 15.6e\n", mod->tEnd);
#endif
            break;
        /**********************/
        default:
            throw_error("Unrecognized card after TIME card in input file.");
            break;
    }
}
