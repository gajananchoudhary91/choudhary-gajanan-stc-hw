#include "global_header.h"

static int DEBUG = ON;
static int DEEP_DEBUG = OFF;

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
void read_SOLVER(MODEL_STRUCT *mod, char *data){
    //int i = 0;               /* Counter */
    //int tempint = 0;         /* Temporary integer */
    int status = 0;          /* Keeping track of read status*/
    double tempdouble = 0.0; /* Temporary double */
    CARD card;               /* For input card detection */

#if D_DEBUG
    if (DEEP_DEBUG){
        printf("Inside read_SOLVER with data=%s", data);
    }
#endif
    card = parse_card(&data);
    switch(card){
        /**********************/
        case CARD_MAXIT:
            throw_error("Card not yet supported. We'll add this later");
            //tempint = read_int_field(&data, &status);
            //if (READ_SUCCESS == status) mod->maxit = tempint;
            //else throw_error("Error reading solver max iterations");
#ifdef D_DEBUG
            //if (DEBUG) printf("Solver max iterations = % 4i\n", mod->maxit);
#endif
            break;
        /**********************/
        case CARD_TOL:
            tempdouble = read_dbl_field(&data, &status);
            if (READ_SUCCESS == status) mod->tol = tempdouble;
            else throw_error("Error reading solver tolerance");
#ifdef D_DEBUG
            if (DEBUG) printf("Solver tolerance      = % 15.6e\n", mod->tol);
#endif
            break;
        /**********************/
        case CARD_FEULER:
            mod->flag.SOLVER_TYPE = CARD_FEULER;
#ifdef D_DEBUG
            if (DEBUG) printf("Using Explicit (f.Euler) time stepping\n");
#endif
            break;
        /**********************/
        case CARD_BEULER:
            mod->flag.SOLVER_TYPE = CARD_BEULER;
#ifdef D_DEBUG
            if (DEBUG) printf("Using Implicit (b.Euler) time stepping\n");
#endif
            break;
        /**********************/
        case CARD_RK2:
            mod->flag.SOLVER_TYPE = CARD_RK2;
#ifdef D_DEBUG
            if (DEBUG) printf("Using Runge-Kutta-2 time stepping\n");
#endif
            break;
        /**********************/
        case CARD_RK4:
            mod->flag.SOLVER_TYPE = CARD_RK4;
#ifdef D_DEBUG
            if (DEBUG) printf("Using Runge-Kutta-4 time stepping\n");
#endif
            break;
        /**********************/
        case CARD_RKF45:
            mod->flag.SOLVER_TYPE = CARD_RKF45;
#ifdef D_DEBUG
            if (DEBUG) printf("Using Runge-Kutta-Fehlberg(4, 5) "
                              "time stepping\n");
#endif
            break;
        /**********************/
        default:
            throw_error("Unsupported solver type");
            break;
    }
}
