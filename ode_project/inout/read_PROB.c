#include "global_header.h"

static int DEBUG = ON;
static int DEEP_DEBUG = OFF;

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
void read_PROB(MODEL_STRUCT *mod, char *data, FILE *fin){
    int i = 0;               /* Counter */
    //int tempint = 0;         /* Temporary integer */
    int status = 0;          /* Keeping track of read status*/
    double tempdouble = 0.0; /* Temporary double */
    //char line[MAXLINE];      /* Stores input file lines */
    CARD card;               /* For input card detection */

#ifdef D_DEBUG
    if (DEEP_DEBUG) printf("Inside read_PROB() with data=%s", data);
#endif
    card = parse_card(&data);
    switch(card){
        /**********************/
        case CARD_IC:
            for (i=0; i<mod->nvar; i++){
                tempdouble = read_dbl_field(&data, &status);
                if (READ_SUCCESS == status) mod->sol_old[i] = tempdouble;
                else throw_error("Error reading ICs");
            }
#ifdef D_DEBUG
            if (DEBUG) {
                printf("Printing initial conditions below:\n");
                for (i=0; i<mod->nvar; i++){
                    printf("sol_old[%3i] = % 15.6e\n", i, mod->sol_old[i]);
                }
            }
#endif
            break;
        /**********************/
        case CARD_KTYPE:
            read_matrix2d("stiffness matrix", mod->K, mod->nvar, data, fin);
            break;
        /**********************/
        case CARD_MTYPE:
            /* Card MTYPE is not yet supported! Throw error */
            throw_error("MTYPE not supported. We'll add this in the future.");
            //read_matrix2d("mass matrix", mod->M, mod->nvar, data, fin);
            break;
        /**********************/
        default:
            /* Do nothing */
            break;
    }
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
void read_matrix2d(char *name, double **mat, int nvar, char *data, FILE *fin){
    int i = 0, j = 0;        /* Counter */
    int tempint = 0;         /* Temporary integer */
    int tempint1 = 0;        /* Temporary integer */
    int tempint2 = 0;        /* Temporary integer */
    int status = 0;          /* Keeping track of read status*/
    double tempdouble = 0.0; /* Temporary double */
    char line[MAXLINE];      /* Stores input file lines */
    CARD card;               /* For input card detection */

#ifdef D_DEBUG
    if (DEEP_DEBUG) printf("Inside read_matrix2d() with data=%s", data);
#endif

    card = parse_card(&data);
    switch (card){
        /**********************/
        case CARD_DENSE:
            for (i=0; i<nvar; i++){
                fgets(line, MAXLINE, fin);
                data = line;
                for (j=0; j<nvar; j++){
                    tempdouble = read_dbl_field(&data, &status);
                    if (READ_SUCCESS == status)
                        mat[i][j] = tempdouble;
                    else
                        throw_error("Error reading 2D matrix");
                }
            }
            break;
        /**********************/
        case CARD_SPARSE:
            tempint = read_int_field(&data, &status);
            if (READ_SUCCESS != status)
                throw_error("Error reading card SPARSE #rows");
                for (i=0; i<tempint; i++){
                    fgets(line, MAXLINE, fin);
                    data = line;

                    tempint1 = read_int_field(&data, &status);
                    if (READ_SUCCESS != status)
                        throw_error("Error reading 2D matrix");

                    tempint2 = read_int_field(&data, &status);
                    if (READ_SUCCESS != status)
                        throw_error("Error reading 2D matrix");

                    if (  (0 > tempint1 || tempint1 >= nvar)
                       && (0 > tempint2 || tempint2 >= nvar) )
                        throw_error("Error reading 2D matrix indices");

                    tempdouble = read_dbl_field(&data, &status);
                    if (READ_SUCCESS == status)
                        mat[tempint1][tempint2] = tempdouble;
                    else
                        throw_error("Error reading 2D matrix");
                }
            break;
        /**********************/
        case CARD_IDENTY:
            for(i=0; i<nvar; i++) mat[i][i] = 1.0; /* Identity */
            break; 
        /**********************/
        default: 
            /* Do nothing */
            break;
    }
#ifdef D_DEBUG
    if (DEBUG) {
        printf("Printing %s below:\n", name);
        for (i=0; i<nvar; i++){
            printf("Row %4i:", i);
            for (j=0; j<nvar; j++){
                printf("    % 15.6e", mat[i][j]);
            }
            printf("\n");
        }
    }
#endif

}
