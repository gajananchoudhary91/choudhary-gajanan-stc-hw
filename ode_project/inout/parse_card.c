#include <ctype.h>
#include "global_header.h"

static int DEBUG = OFF;

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
CARD parse_card(char **outstr){
    char *i = *outstr;
    unsigned int cardvalue = 0, pow36 = 1, value = 0;
    remove_spaces(&i);
    *outstr = i;
#ifdef D_DEBUG
    if (DEBUG) {
        PRINT_FEW_STARS();
        printf("String before parsing card: \"%s\"\n",*outstr);
    }
#endif
    while (0 != *i && (' ' != *i && '\t' != *i && '\n' != *i)){
        if (('A' <= *i) && ('z' >= *i)) {
            value = toupper(*i)-'A';
#ifdef D_DEBUG
            if (DEBUG) printf("\nAlphabet %c, Value = %i - %i = %i", toupper(*i), toupper(*i), 'A', value);
#endif
            cardvalue += value*pow36;
            pow36 *= 36;
        }
        else if (('0' <= *i) && ('9' >= *i)) {
            value = 27 + *i - '0';
#ifdef D_DEBUG
            if (DEBUG) printf("\nNumber   %c, Value = 27 + %i - %i = %i", *i, *i, '0', value);
#endif
            cardvalue += value*pow36;
            pow36 *= 36;
        }
        else {
           cardvalue = CARD_NO;
           break;
        }
        i++;
    }
    if (0 == cardvalue) cardvalue = CARD_NO;
    *outstr = i;
    /* remove_spaces(&i); */

    return (CARD) cardvalue;
}
