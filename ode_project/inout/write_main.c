#include "global_header.h"

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
void write_main(double t, double *sol, int nvar, char *name){
    int i=0;
    char outfilename[MAXLINE];
    FILE *outfile=NULL;
    sprintf(outfilename, "%s-sol.dat", name);
    outfile = fopen(outfilename,"a");
    if (NULL==outfile)
        throw_error("Error opening output file for writing.");

    fprintf(outfile,"  % 15.6e", t);
    for (i=0; i<nvar; i++) fprintf(outfile,",  % 15.6e", sol[i]);
    fprintf(outfile, "\n");

    fclose(outfile);
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
void write_initial_output(double t, double *sol, int nvar, char *name){
    int i=0;
    char outfilename[MAXLINE];
    FILE *outfile=NULL;
    sprintf(outfilename, "%s-sol.dat", name);
    outfile = fopen(outfilename,"w");
    if (NULL==outfile)
        throw_error("Error opening output file for writing.");

    /* Topmost line of output file. */
    fprintf(outfile, "             Time");
    for (i=0; i<nvar; i++) fprintf(outfile,",     Variable %3i", i);
    fprintf(outfile, "\n");

    /* Write the initial condition. */
    fprintf(outfile,"  % 15.6e", t);
    for (i=0; i<nvar; i++) fprintf(outfile,",  % 15.6e", sol[i]);
    fprintf(outfile, "\n");

    fclose(outfile);
}
