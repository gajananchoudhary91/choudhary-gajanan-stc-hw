#include "global_header.h"

static int DEBUG = ON;
static int DEEP_DEBUG = OFF;

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
int read_main(MODEL_STRUCT **model_ptr, char *model_name){
    int ierr = SUCCESS;      /* Error tracker */
    int nvar = 0;            /* Number of variables in the ODE */
    //int i = 0;               /* Counter */
    int tempint = 0;         /* Temporary integer */
    int status = 0;          /* Keeping track of read status*/
    //double tempdouble = 0.0; /* Temporary double */
    char filename[MAXLINE];  /* Input file name */
    char line[MAXLINE];      /* Stores input file lines */
    char *data;              /* Char pointer for I/O operations */
    FILE *fin;               /* Input file pointer */
    CARD card;               /* For input card detection */
    MODEL_STRUCT *mod;       /* Alias pointer for the model */
    
    sprintf(filename, "%s.dat", model_name);
    fin = fopen(filename, "r");
    if (NULL == fin) {
        printf("Could not open input file %s for reading\n", filename);
        exit(-1);
    }
    
    /* Preliminary parsing for memory allocation. */
    while (NULL != fgets(line, MAXLINE, fin)) {
        data = line;
        card = parse_card(&data);
#ifdef D_DEBUG
        if (DEEP_DEBUG)
            printf("line = %sCard value = %10u\n", line, (unsigned int) card);
#endif
        switch(card){
            /**********************/
            case CARD_PROB:
                card = parse_card(&data);
                switch(card){
                    case CARD_NVAR:
                        tempint = read_int_field(&data, &status);
                        if (READ_SUCCESS == status) nvar = tempint;
                        else throw_error("Error reading NVAR <int field>.");
                        if (0 >= nvar)
                            throw_error("Expected positive int field NVAR");
#ifdef D_DEBUG
                        if (DEBUG) printf("nvar = %i", nvar);
#endif
                        break;
                    default:
                        /* Do nothing */
                        break;
                }
                break;
            /**********************/
            default:
                /* Do nothing */
                break;
        }
    }
    
    if (0 >= nvar){
        throw_error("Error: Number of variables not specified."
                    "Line \"PROB NVAR <#Vars>\" required in input file.");
    }

    ierr = model_alloc_init(model_ptr, nvar, model_name);
    if (FAILURE == ierr){
        printf("Failed somewhere in function model_alloc().");
        return ierr;
    }
    mod = *model_ptr;
    flag_defaults(&(mod->flag));

    rewind(fin);
    while (NULL != fgets(line, MAXLINE, fin)) {
        data = line;
        card = parse_card(&data);
#ifdef D_DEBUG
        if (DEEP_DEBUG)
            printf("line = %sCard value = %10u\n", line, (unsigned int) card);
#endif
        switch(card){
            /**********************/
            case CARD_PROB:
                read_PROB(mod, data, fin);
                break;
            /**********************/
            case CARD_TIME:
                read_TIME(mod, data);
                break;
            /**********************/
            case CARD_SOLVER:
                read_SOLVER(mod, data);
                break;
            /**********************/
            default:
                /* Do nothing */
                break;
        }
    }

    /* Set model time for starting time steps. */
    mod->t_curr = mod->t_prev + mod->dt;
    
    fclose(fin);
    
    return ierr;
}
