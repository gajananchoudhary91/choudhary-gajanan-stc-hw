# General source files (always included)
list(APPEND inout_srcs
    # Insert new files below here, alphabetically
    parse_card.c
    read_fields.c
    read_PROB.c
    read_TIME.c
    read_SOLVER.c
    read_main.c
    write_main.c
    )

list(REMOVE_DUPLICATES inout_srcs)

list(APPEND lib_depends ode_structs)

add_library("ode_inout" STATIC EXCLUDE_FROM_ALL ${inout_srcs})
target_link_libraries(ode_inout ${lib_depends})
