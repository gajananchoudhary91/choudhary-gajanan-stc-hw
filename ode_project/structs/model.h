/*! \file model.h
 * \brief Contains definition of data structure MODEL_STRUCT.
 * 
 * \details
 * MODEL_STRUCT is the main data structure used in this project. All 
 * variables related to an ODE system being simulated are stored in this 
 * data structure.
 *
 * \param nvar       Total number of variables.
 * \param flag       Flags related to the model - see FLAG_STRUCT.
 * \param dt         Time step.
 * \param t_curr     Current time we're solving for.
 * \param t_prev     Previous time we know the solution for.
 * \param tStart     Starting time.
 * \param tEnd       Ending time.
 * \param max_dt     Max time step = user supplied dt.
 * \param tol        Solver tolerance.
 * \param sol        1D Solution vector of current time step.
 * \param sol_old    1D Solution vector of previous time step.
 * \param K          2D Matrix in x'=Kx.
 * \param gsl_system ODE system used in GSL.
 * \param gsl_driver ODE driver used in GSL.
 *
 * */

#ifndef MODEL_HEADER_H
#define MODEL_HEADER_H

typedef struct {
    int nvar;         /* Total number of variables */
    FLAG_STRUCT flag;

    /* Time */
    double dt;        /* Time step */
    double t_curr;    /* Current time we're solving for */
    double t_prev;    /* Previous time we know the solution for */
    double tStart;    /* Starting time */
    double tEnd;      /* Ending time */
    double max_dt;    /* Max time step = user supplied dt */

    /* Solver  */
    double tol;       /* Solver tolerance */
    
    double *sol;      /* Solution vector of current time step */
    double *sol_old;  /* Solution vector of previous time step */
    double **K;       /* Right matrix in Mx'=Kx */

#ifdef D_GSL
    gsl_odeiv2_system  gsl_system; /* */
    gsl_odeiv2_driver *gsl_driver; /* */
#endif

    char model_name[MAXLINE]; /* Model name - same as input arg. */

    /* Future proofing: Some functionality already added and commented */
    /* throughout the code. */
    // int maxit;        /* Maximum number of solver iterations. */
    // double dt_old;    /* Previous time step */
    // double *ic;       /* Initial condition on the solution */
    // double *rhs;      /* Right hand side in Ax=b */
    // double **M;       /* Left matrix in Mx'=Kx */
} MODEL_STRUCT;

int model_alloc_init(MODEL_STRUCT **, int, char *);
int model_free(MODEL_STRUCT **);
int model_check_update_time(MODEL_STRUCT *);

#endif /* #ifndef MODEL_HEADER_H */
