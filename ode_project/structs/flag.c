/*! \file flag.c
 * \brief File setting default values of the FLAG_STRUCT data structure.
 * */

#include "define.h"
#include "flag.h"
#include "cards.h"

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/*! \fn void flag_defaults(FLAG_STRUCT *flag)
 * \brief Sets the default values of the flags in FLAG_STRUCT.
 * \param flag    Pointer to the flag structure being set.
 * */
void flag_defaults(FLAG_STRUCT *flag) {
    flag->ADAPT_TIME = NO;
    flag->SOLVER_TYPE = CARD_FEULER;

}
