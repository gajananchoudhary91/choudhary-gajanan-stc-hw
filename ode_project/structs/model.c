/*! \file model.c
 * \brief Functions only related to MODEL_STRUCT.
 *
 * \details
 * This file contains functions that are strictly related to the MODEL_STRUCT 
 * data structure. Currently, it contains functions to allocate, initialize, 
 * and free the struct, as well as a function that updates the current time 
 * and time step, and checks if the simulation has completed.
 *
 * */

/****************************************************************************/
#include "global_header.h"

static int DEBUG = OFF;

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/*! \fn int model_alloc_init(MODEL_STRUCT **, int, char *)
 * \brief Allocates and initializes (a single) model struct.
 * \param model_ptr  Pointer to the address of the MODEL_STRUCT pointer.
 * \param nvar       Number of variables in the simulation.
 * \param model_name Name of the input file (except for the *.dat part).
 * \return SUCCESS   If allocation and initialization was successful.
 * */
int model_alloc_init(MODEL_STRUCT **model_ptr, int nvar, char *model_name) {
    int i;             /* Counter */
    MODEL_STRUCT *mod; /* Alias pointer for model */
    (*model_ptr) = (MODEL_STRUCT *)malloc(sizeof(MODEL_STRUCT));
    checkmem(*model_ptr);
    mod = *model_ptr;

    //mod->ic = (double *) malloc(sizeof(double) * nvar);
    //checkmem(mod->ic);

    //mod->rhs = (double *) malloc(sizeof(double) * nvar);
    //checkmem(mod->rhs);

    mod->sol = (double *) malloc(sizeof(double) * nvar);
    checkmem(mod->sol);

    mod->sol_old = (double *) malloc(sizeof(double) * nvar);
    checkmem(mod->sol_old);

    /* Not supported yet */
    // /* Allocating M */
    //mod->M = (double **) malloc(sizeof(double *) * nvar);
    //checkmem(mod->M);
    //mod->M[0] = (double *) malloc(sizeof(double) * nvar * nvar);
    //checkmem(*(mod->M));
    //for (i=0; i<nvar; i++) {
    //    mod->M[i] = *(mod->M) + i*nvar;
    //}

    /* Allocating K */
    /* DO NOT change this malloc. Need K to be contiguous in memory for GSL */
    mod->K = (double **) malloc(sizeof(double *) * nvar);
    checkmem(mod->K);
    mod->K[0] = (double *) malloc(sizeof(double) * nvar * nvar);
    checkmem(*(mod->K));
    for (i=0; i<nvar; i++) {
        mod->K[i] = *(mod->K) + i*nvar;
    }

    mod->dt     = 0.0;
    mod->t_curr = 0.0;
    mod->t_prev = 0.0;
    mod->t_prev = 0.0;
    mod->tStart = 0.0;
    mod->tEnd   = 0.0;
    mod->tol    = 0.0;
    mod->nvar   = nvar;
    sprintf(mod->model_name, "%s", model_name);
#ifdef D_GSL
    mod->gsl_driver = NULL;
#endif

    return SUCCESS;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/*! \fn int model_free(MODEL_STRUCT **)
 * \brief Frees the memory of a single model struct.
 * \param model_ptr  Pointer to the address of the MODEL_STRUCT pointer.
 * \return SUCCESS   If freeing was successful.
 * */
int model_free(MODEL_STRUCT **model_ptr) {
    if (NULL != *model_ptr && 0 < (*model_ptr)->nvar){
        //if (NULL != (*model_ptr)->ic)        free((*model_ptr)->ic);
        //if (NULL != (*model_ptr)->rhs)       free((*model_ptr)->rhs);
        if (NULL != (*model_ptr)->sol)       free((*model_ptr)->sol);
        if (NULL != (*model_ptr)->sol_old)   free((*model_ptr)->sol_old);
        if (NULL != (*model_ptr)->K[0])      free((*model_ptr)->K[0]);
        if (NULL != (*model_ptr)->K)         free((*model_ptr)->K);
        /* Not supported yet */
        //if (NULL != (*model_ptr)->M[0])      free((*model_ptr)->M[0]);
        //if (NULL != (*model_ptr)->M)         free((*model_ptr)->M);
    }

#ifdef D_GSL
    if (NULL != (*model_ptr)->gsl_driver)
        gsl_odeiv2_driver_free((*model_ptr)->gsl_driver);
#endif

    if (NULL != *model_ptr) free(*model_ptr);
    *model_ptr = NULL;

    return SUCCESS;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/*! \fn int model_check_update_time(MODEL_STRUCT *)
 * \brief Allocates and initializes (a single) model struct.
 * \details
 * This function returns a YES if the user-supplied end time is reached, 
 * which flags the simulation to be ended. If the end time has not been 
 * reached, the function updates the current time and the time step, and 
 * returns NO.
 *
 * \param mod    Pointer to the model.
 * \return NO    Returns NO till the end time of simulation is reached.
 * */
int model_check_update_time(MODEL_STRUCT *mod) {

    if(mod->t_curr >= mod->tEnd){
        return YES;
    }

    if (mod->flag.ADAPT_TIME==YES) { /* Adaptive time step */
        throw_error("Adaptive time stepping not yet supported");
        /* Cut time step if solver did not converge, else increase it. */
        //if (0){
        //    mod->dt /= DT_REDUCE_FACTOR;
        //}
        //else if ((mod->dt*DT_INCREASE_FACTOR <= mod->max_dt + SMALL)) {
        //    mod->dt *= DT_INCREASE_FACTOR;
        //}
    }
    else if (mod->flag.ADAPT_TIME==NO) { /* Fixed time step */
        /* The factor 1.1 below is to keep some room for changing dt */
        /* during the last time step, to prevent rounding errors or */
        /* extra time steps by mistake */
        if (mod->t_curr + 1.1 * mod->dt < mod->tEnd){ /* Most time steps */
            mod->t_curr += mod->dt;
        }
        else { /* Last time step */
            mod->dt = mod->tEnd - mod->t_curr;
            mod->t_curr = mod->tEnd;
        }
    }

#ifdef D_DEBUG
    if (DEBUG)
        printf("Time step at end of model_update_time() = %15.6e\n", mod->dt);
#endif
    return NO;
}
