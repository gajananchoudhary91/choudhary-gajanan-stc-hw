/*! \file flag.h
 * \brief Data structure determining model behaviour MODEL_STRUCT.
 *
 * \details
 * FLAG_STRUCT contains flags that are used in \a if and \a switch 
 * conditions during runtime to control the behavior of the model. These 
 * flags are set during input parsing.
 *
 * \param ADAPT_TIME  Flag to check if user allowed temporal adaption.
 * \param SOLVER_TYPE Flag to select a solver, set a solver card in cards.h.
 * 
 * */

#ifndef FLAG_HEADER_H
#define FLAG_HEADER_H

typedef struct {
    int ADAPT_TIME;         /* YES if adaptive time step. */
    int SOLVER_TYPE;        /* Explicit/RK2/RK4/RKF45, set using cards */

} FLAG_STRUCT;

void flag_defaults(FLAG_STRUCT *);

#endif /* #ifndef FLAG_HEADER_H */
