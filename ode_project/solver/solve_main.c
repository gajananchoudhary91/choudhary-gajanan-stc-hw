#include "global_header.h"

static int DEBUG = ON;
static int DEEP_DEBUG = OFF;

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
int solve_main(MODEL_STRUCT *mod){
    int ierr = SUCCESS;      /* Error tracker */
    int status = FAILURE;    /* GSL error tracker */
    int i = 0;               /* Counter */
    int nvar = mod->nvar;    /* Number of variables in the ODE */
    
#ifdef D_DEBUG
    if (DEEP_DEBUG) printf("Inside solve_main(), time %15.6e\n", mod->t_curr);
#endif

    /* Recall: the problem we're solving is: dy/dt = K*y */
    /**********************/
    if (CARD_FEULER == mod->flag.SOLVER_TYPE){
        /* Numerical method: y_(n+1) = y_(n) + (dt)*K*y_(n) */
        /* First do: K*y_(n)*/
        tl_dbl_mat_array_prod(mod->K, mod->sol_old, nvar, nvar, mod->sol);
        /* Now do: y_(n)+(dt)*K*y(n) */
        for (i=0; i<nvar; i++) mod->sol[i] = mod->sol[i]*mod->dt
                                           + mod->sol_old[i];
        mod->t_prev = mod->t_curr;
    }
    /**********************/
    else if (  CARD_RK2   == mod->flag.SOLVER_TYPE
            || CARD_RK4   == mod->flag.SOLVER_TYPE
            || CARD_RKF45 == mod->flag.SOLVER_TYPE ){
#ifdef D_GSL
        status = gsl_odeiv2_driver_apply(mod->gsl_driver,
                     &(mod->t_prev), mod->t_curr, mod->sol);
        if (GSL_SUCCESS != status){
            printf("GSL solver error: return value = %d\n", status);
            ierr = FAILURE;
        }
        /* Copy solution to old_solution */
        tl_dbl_copy_array(mod->sol, nvar, mod->sol_old);
#else
        /* Not required since we have this in ode_initialize, but still... */
        printf("Error. Code MUST be compiled with GSL to use RK methods.\n");
        return FAILURE;
#endif
    }
    /**********************/
    else if (CARD_BEULER == mod->flag.SOLVER_TYPE){
        /* Numerical method: (I-(dt)*K)*y_(n+1) = y_(n) */
        throw_error("Implicit/Backward Euler not yet supported.");
    }
    /**********************/
    else {
        throw_error("Unsupported solver type");
    }

    /* If the time-stepping failed */
    if (SUCCESS != ierr){
        printf("Time stepping failed.\n");
        return ierr;
        /* Copy old_solution to solution */ /* Check later */
        //tl_dbl_copy_array(mod->sol_old, nvar, mod->sol);
    }


    /* Prepare the next time step */
#ifdef D_DEBUG
    if(DEBUG) {
        //tl_dbl_print_array(mod->sol_old, nvar, "solution_old");
        tl_dbl_print_array(mod->sol, nvar, "solution");
    }
#endif
    /* Copy solution to old_solution */
    tl_dbl_copy_array(mod->sol, nvar, mod->sol_old);
    
    /* Reset to zero */ /* Not required yet */
    // tl_dbl_init_array(mod->sol, nvar);

#ifdef D_DEBUG
    if(DEEP_DEBUG) {
        printf("Check that sol_old is set to sol, "
               "and sol is set to zero:\n");
        tl_dbl_print_array(mod->sol_old, nvar, "solution_old");
        tl_dbl_print_array(mod->sol, nvar, "solution");
    }
#endif

    return ierr;
}
