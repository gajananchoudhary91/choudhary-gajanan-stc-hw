#include "global_header.h"

static int DEBUG=ON;

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
int rhs_func(double t, const double *y, double *f, void *params){
    (void)(t);        /* Avoid unused parameter warning */
    MODEL_STRUCT *mod = (MODEL_STRUCT *) params;

    /* Set RHS = {f} = [K]*{y} */
    tl_dbl_mat_array_prod(mod->K, y, mod->nvar, mod->nvar, f);

    return GSL_SUCCESS;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
int rhs_jac(double t, const double *y, double *dfdy, double *dfdt, 
            void *params){
    (void)(t);        /* Avoid unused parameter warning */
    (void)(y);        /* Avoid unused parameter warning */
    MODEL_STRUCT *mod = (MODEL_STRUCT *) params;

    /* Set row major Jac[i][j] = dfdy[i*nvar + j] */
    /* Warning: The following step requires K to be contiguous in memory. */
    /*          This means K should be set using a single malloc. */
    dfdy = *(mod->K);
    (void)(dfdy);     /* Avoid unused parameter warning */
    tl_dbl_init_array(dfdt, mod->nvar);
    return GSL_SUCCESS;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
int gsl_alloc_init(MODEL_STRUCT *mod){
    int status = FAILURE;

    /* set up the GSL system */
    mod->gsl_system.function = rhs_func;
    mod->gsl_system.jacobian = rhs_jac;
    mod->gsl_system.dimension = (size_t) mod->nvar;
    mod->gsl_system.params = (void *) mod;

#ifdef D_DEBUG
    if (DEBUG){
        printf("mod->gsl_system.params->nvar    = %i\n", 
               ((MODEL_STRUCT *)mod->gsl_system.params)->nvar);
        printf("mod->gsl_system.params->K[0][0] = % 15.6e\n", 
               ((MODEL_STRUCT *)mod->gsl_system.params)->K[0][0]);
    }
#endif

    /* Set up the GSL driver */
    switch (mod->flag.SOLVER_TYPE){
        /**********************/
        case CARD_RK2:
            mod->gsl_driver = gsl_odeiv2_driver_alloc_y_new(
                              &(mod->gsl_system), gsl_odeiv2_step_rk2, 
                              mod->dt, mod->tol,0.0);
            break;
        /**********************/
        case CARD_RK4:
            mod->gsl_driver = gsl_odeiv2_driver_alloc_y_new(
                              &(mod->gsl_system), gsl_odeiv2_step_rk4, 
                              mod->dt, mod->tol,0.0);
            break;
        /**********************/
        case CARD_RKF45:
            mod->gsl_driver = gsl_odeiv2_driver_alloc_y_new(
                              &(mod->gsl_system), gsl_odeiv2_step_rkf45, 
                              mod->dt, mod->tol,0.0);
            break;
        /**********************/
        default:
            break;
    }
    
    /* Set up additional GSL parameters */
    status = gsl_odeiv2_driver_set_hmin(mod->gsl_driver, SMALL);
    if (GSL_SUCCESS != status) printf("Error setting GSL minimum time step");
    status = gsl_odeiv2_driver_set_hmax(mod->gsl_driver, mod->max_dt);
    if (GSL_SUCCESS != status) printf("Error setting GSL maximum time step");

    /* GSL requires the initial conditions to be set in the solution vector */
    tl_dbl_copy_array(mod->sol_old, mod->nvar, mod->sol);
    
    if (GSL_SUCCESS == status) return SUCCESS;
    else                       return FAILURE;
}

/****************************************************************************/
