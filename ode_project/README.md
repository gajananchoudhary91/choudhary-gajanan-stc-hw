# README #

This project solves a system of first order linear ordinary differential 
equations in time. The code is in C, and we use CMake for compiling it.

### Instructions for project evaluation: Outputs, scripts etc. ###
To make your life easy (hopefully), I've written some scripts. There are 
python scripts located in the `ode_project/scripts` folder that plot the
solution for specific problems that I have set up in the `verification` 
folder. There is a master script for compiling, testing, running, plotting,
and cleaning located in the `ode_projet/CMAKE/bin/` folder, called `scrip.sh`.
This is the most important one that you will hopefully use without any problems.
To run this script in the command line _on Stampede_,

0. Set your current directory to `ode_project/CMAKE/bin` folder.
1. Open the script `scrip.sh`
2. Find the following if condition after the top comments section of the file, 
   and replace `"$USER"="gajanan"` with your own user name, and set the 
   `project_root_path` below it to the `ode_project` folder, without the ending `/`,
   similar to the way it is currently set. It MUST be the _absolute path_ and 
   not the relative one, or else, bad things will happen.
   
   if [ "$HOSTNAME" = "harvey.ices.utexas.edu" ]; then                # ICES
      
   elif [ "$USER" = "gajanan" ]; then                             # Stampede
   
      project_root_path="/work/03194/gajanan/stampede2/choudhary-gajanan-stc-hw/ode_project"
    
   else
      
   fi
   
3. Run the script on the command line as `./scrip.sh` for no testing or `./scrip.sh t`
   to run with unit testing (Recommended. These unit tests take an extra 2-3 seconds to run!)
   This will turn on the unit testing mode. The program will compile, test, run, and plot
   the unit tests and the two problems in the `ode_project/CMAKE/bin/verification` folder
   and the `ode_project/CMAKE/bin/unit_test_output` folders, respectively. It is the plots
   obtained from this script that I have placed in the `ode_project/scripts/` folder.
4. To turn on profiling, use `./scrip.sh  p`. After the programs are run, you can use 
   gprof on the binary in the `ode_project/CMAKE/bin/verification` folder to get the profile.
5. Coverage doesn't work on stampede, but is possible on a local desktop. However, with 
   cmake, coverage is a bit tedious and currently has to be done by hand on each file. I 
   have not yet added this automation in my script.
   


### Instructions: Compiling ###
Create a directory, `CMAKE`, in the project root directory `ode_project`. 
Set your current directory to `ode_project/CMAKE/` in the terminal and run 
`cmake <optional_arguments> ..` followed by `make`. If testing mode is 
turned on, you can run tests using `ctest` command. The test outputs will 
be stored in `ode_project/CMAKE/tests` and `ode_projects/CMAKE/Testing/Temporary`
folders.

The following are the optional arguments for cmake command:

0. -DUSE\_GSL={ON, OFF} Use GSL. Keep this ON, or some of the scripts may not 
   run properly.
1. -DTESTING={ON, OFF} Unit testing. Recommended: ON
2. -DDEBUG={ON, OFF} Debug mode. Turn this on if something goes wrong or if the 
   program starts throwing errors/segmentation faults
3. -DPROFILING={ON, OFFF} Profiling using gprof.
4. -DCOVERAGE={ON, OFF} Coverage using gcov. Does not work on Stampede
5. -DUSE\_COLOR\_OUTPUT={ON, OFF} Color some output for the terminal for ease of 
   viewing.


### Instructions: Running simulations ###
On a successful compilation, the executable `ode` will be created in the 
directory `ode_project/CMAKE/bin/`. In case testing is turned on, additional 
binaries will also be copied in that folder. Copy your input file, say 
`input.dat` to this folder, set your current directory in the terminal to 
this folder, and run the command `./ode input` (without the `*.dat` extension). 
On a successful run, the program will print output files.


### Instructions: Documentation ###
We use Doxygen to generate the documentation for this project.
To create the documentation, simply run the script `documentation.sh` placed in 
the `ode_project` folder. This will create the HTML documentation and open the 
main `index.html` file.

### Information: Code arrangemenet and directory structure ###
The code is divided into the following directories:

1. `ode_project` This is the root directory containing the entire project.
2. `main` This folder contains the main driver functions of the project.
3. `include` This folder contains the main header files of the project.
4. `structs` This folder contains the data structures used in this project.
5. `inout` This folder contains the input/output related files of the project.
6. `solver` This folder contains the time stepping routines.
7. `tools` This folder contains general tools, such as matrix-vector product.
8. `tests` This folder contains the unit tests of the project.
9. `verification` this folder contains the 2 problems that were asked in the
   project requirements, viz. the 'simple' problem `problem1` and the problem of
   particle in an electric field, `problem2`. There are 8 input files in total,
   4 each for the various timestepping methods.
10. `scripts` This folder contains 4 python scripts for plotting solutions, and
    the output from the scripts, i.e., the solution to problems 1 and 2, along 
    with the convergence plots of the time steppers.
11. `CMAKE` This folder is intended to be the build directory. The binary will be
    placed in the `CMAKE/bin` folder, where I have also placed a master script
    for your convenience, `scrip.sh`, that compiles, tests, runs, plots, and cleans
    up the project.


### Instructions: Problems/Questions? ###
If there are any problems with the project, scripts, or modules, please feel free
to email me at  gajanan<AT>utexas.edu.