/*! \file fnctn.h 
 * \brief Includes general function declarations.
 *
 * */
#ifndef FNCTN_HEADER_H
#define FNCTN_HEADER_H

/****************************************************************************/
/****************************************************************************/
/* main */
int ode_initialize(MODEL_STRUCT **, char *);
int ode_run(MODEL_STRUCT *);
int ode_finalize(MODEL_STRUCT **);

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/* inout */
void remove_spaces(char **);
int read_int_field(char **, int *);
double read_dbl_field(char **, int *);
CARD parse_card(char **);
void read_PROB(MODEL_STRUCT *, char *, FILE *);
void read_matrix2d(char *, double **, int , char *, FILE *);
void read_TIME(MODEL_STRUCT *, char *);
void read_SOLVER(MODEL_STRUCT *, char *);
int read_main(MODEL_STRUCT **, char *);
void write_main(double, double *, int, char *);
void write_initial_output(double t, double *, int, char *);

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/* solver */
int solve_main(MODEL_STRUCT *);
#ifdef D_GSL
int rhs_func(double, const double *, double *, void *);
int rhs_jac(double, const double *, double *, double *, void *);
int gsl_alloc_init(MODEL_STRUCT *);
#endif

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/* tools */
void tl_dbl_init_array(double *, int);
void tl_dbl_copy_array(const double *, int, double *);
void tl_dbl_array_dot_prod(const double *, const double *, int, double *);
void tl_dbl_mat_array_prod(double **, const double *, int, int, double *);
void tl_dbl_print_array(const double *, int, const char *);

#endif /* #ifndef FNCTN_HEADER_H */
