/*! \file macro.h 
 * \brief File containing macros used in the code.
 * */

#ifndef MACRO_HEADER_H
#define MACRO_HEADER_H

#define PRINT_MANY_STARS()                             \
    printf("**************************************"    \
           "*************************************\n");

#define PRINT_FEW_STARS()                              \
    printf("****************************************\n");

#define PRINT_VERY_FEW_STARS() printf("********************\n");

#define PRINT_TEST_OUTCOME(ierr, s) \
    if (SUCCESS==ierr) printf(COLOR_GREEN "SUCCESS" COLOR_RESET ": " s "\n");\
    else printf(COLOR_RED "FAILURE" COLOR_RESET ": " s "\n");

#define MAX(a, b) (((a) > (b)) ? (a) : (b))

#define MIN(a, b) (((a) < (b)) ? (a) : (b))

#define throw_error(s) {\
    PRINT_MANY_STARS(); \
    printf("Error: File \"%s\", function \"%s\", line %d.\n", \
           __FILE__, __func__, __LINE__); \
    printf(s "\n"); \
    printf(COLOR_RED "Program exited with FAILURE" COLOR_RESET "\n"); \
    PRINT_MANY_STARS(); printf("\n\n"); \
    exit(EXIT_FAILURE);}

#define checkmem(ptr) {\
    if (NULL == (ptr)){ \
        throw_error("Error: Could not allocate memory properly"); \
    }}

#define CHECKSUCCESS(ierr) {\
    if (SUCCESS != (ierr)){ \
        PRINT_MANY_STARS(); \
        printf("Error: File \"%s\", function \"%s\", line %d.\n", \
                __FILE__, __func__, __LINE__); \
        printf(COLOR_RED "Program exited with FAILURE" COLOR_RESET "\n"); \
        PRINT_MANY_STARS(); printf("\n\n"); \
        return EXIT_FAILURE; \
    }}

#endif /* #ifndef MACRO_HEADER_H */

/*! \def PRINT_MANY_STARS() 
 * \brief A macro that prints 75 stars.
 * */

/*! \def PRINT_FEW_STARS() 
 * \brief A macro that prints 40 stars.
 * */

/*! \def PRINT_VERY_FEW_STARS() 
 * \brief A macro that prints 20 stars.
 * */

/*! \def PRINT_TEST_OUTCOME(ierr, s)
 * \brief A macro that prints whether a test was successful.
 * 
 * If a test described by string \a s was a success based on the value of 
 * \a ierr, then this macro prints SUCCESS followed by the string \a s, 
 * else it prints FAILURE followed by the string \a s.
 *
 * */

/*! \def MAX(a, b)
 * \brief A macro that returns the maximum of \a a and \a b.
 * */

/*! \def MIN(a, b)
 * \brief A macro that returns the minimum of \a a and \a b.
 * */

/*! \def throw_error(s)
 * \brief A function that throws an error given by string \a s and exits.
 * */

/*! \def checkmem(ptr)
 * \brief A function that throws an error if a pointer \a ptr is NULL.
 *
 * \details This function is intended for use for checking if memory 
 * allocation after a malloc/calloc call was successful. If not, it throws 
 * an error and exits the program. \a ierr is an integer containing 
 * indicating success or failure. \a s is a user-supplied string (related to 
 * code testing) to be printed.
 *
 * */

/*! \def CHECKSUCCESS(ierr)
 * \brief A macro that stops program with EXIT_FAILURE if \aierr!=\aSUCCESS.
 * */
