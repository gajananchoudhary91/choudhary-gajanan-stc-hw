/*! \file type.h
 * \brief Includes all data structure headers.
 *
 * */
#ifndef TYPE_HEADER_H
#define TYPE_HEADER_H

#include "flag.h"
#include "model.h"

#endif /* #ifndef TYPE_HEADER_H */
