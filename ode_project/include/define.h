/*! \file define.h
 * \brief Includes standard header files and constants.
 *
 * */

#ifndef DEFINE_HEADER_H
#define DEFINE_HEADER_H


/* Standard Header Files*/
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <time.h>
#include <assert.h>
#include <limits.h>


/* Basic definitions */
#define MAXLINE 300

/* Booleans */
#define ON 1
#define OFF 0
#define YES 1
#define NO -3
#define READ_SUCCESS 4
#define READ_FAILURE 0
#define SUCCESS 1
#define FAILURE 0
#define TRUE 1
#define FALSE 0
#define USED 1
#define UNUSED 0

/* Some constants */
#define TINY   DBL_EPSILON   /* Smallest double */
#define SMALL  FLT_EPSILON   /* Smallest float */
#define SMALL1 1.0E-1
#define SMALL2 1.0E-2
#define SMALL3 1.0E-3
#define SMALL6 1.0E-6
#define SMALL9 1.0E-8
#define SMALL12 1.0E-12

#define DT_REDUCE_FACTOR  0.25
#define DT_INCREASE_FACTOR  2.0

/* Convergence rates - verification puposes */
#define FEULER_LAST_CONV_RATE  1.0
#define RK2_LAST_CONV_RATE     3.0
#define RK4_LAST_CONV_RATE     4.0
#define RKF45_LAST_CONV_RATE   5.0

/* Coloring text! */
#ifdef D_COLOR_OUTPUT
    #define COLOR_RED     "\x1b[31m"
    #define COLOR_GREEN   "\x1b[32m"
    #define COLOR_YELLOW  "\x1b[33m"
    #define COLOR_BLUE    "\x1b[34m"
    #define COLOR_MAGENTA "\x1b[35m"
    #define COLOR_CYAN    "\x1b[36m"
    #define COLOR_RESET   "\x1b[0m"
#else
    #define COLOR_RED ""
    #define COLOR_GREEN ""
    #define COLOR_YELLOW ""
    #define COLOR_BLUE ""
    #define COLOR_MAGENTA ""
    #define COLOR_CYAN ""
    #define COLOR_RESET ""
#endif  /* #ifdef D_COLOR_OUTPUT */


#endif  /* #ifndef DEFINE_HEADER_H */

/*! \def MAXLINE
 * \brief Maximum length of a character string in the code.
 * */
/*! \def READ_SUCCESS
 * \brief Return value for successful integer/double read.
 * */
/*! \def READ_FAILURE
 * \brief Return value for failed integer/double read.
 * */
/*! \def SUCCESS
 * \brief Return value for successful function call.
 * */
/*! \def FAILURE
 * \brief Return value for failed function call.
 * */
/*! \def DT_REDUCE_FACTOR
 * \brief Factor for decreasing time step in case of solver failure.
 * */
/*! \def DT_INCREASE_FACTOR
 * \brief Factor for increasing time step in case of solver success.
 * */
