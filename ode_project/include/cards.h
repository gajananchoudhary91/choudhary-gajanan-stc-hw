/*! \file cards.h
 * \brief Contains a CARD enum that is used for reading input files.
 * 
 * \details
 * This file contains an \a enum \a CARD definition that defines cards 
 * as integers using a one-one mapping. By cards, we mean strings in the 
 * input file that the code recognizes as certain types of input. If a card 
 * <somecard> is one of the input file options, then its corresponding 
 * enum is given by \a CARD_<somecard>. On reading a string from the input 
 * file, a card parser function can convert the string to its corresponding 
 * enum value \a cardvalue by the following formula,
 *
 *     cardvalue = card[0] + card[1]*36 + card[2]*36^2 + ...
 *
 * where the character array \a card[] contains the string <somecard> that 
 * is being parsed. Following are the numerous cards.
 *
 * \param CARD_PROB   ODE parameters.
 * \param CARD_TIME   Time parameters.
 * \param CARD_SOLVER Solver parameters.
 * \param CARD_NVAR   Number of variables in ODE.
 * \param CARD_IC     Specifies initial conditions in a single line.
 * \param CARD_MTYPE  Not yet supported. To be enabled in the future.
 * \param CARD_KTYPE  Specifies the \a [K] matrix of the ODE.
 * \param CARD_DENSE  Specifies that the full matrix is supplied elementwise.
 * \param CARD_SPARSE Specifies only non-zero matrix elements index-wise.
 * \param CARD_IDENTY Sets the matrix to identity.
 * \param CARD_TSTART Specifies the starting time of the ODE.
 * \param CARD_TEND   Specifies the ending time of the ODE.
 * \param CARD_DT     Specifies the time step.
 * \param CARD_FIXED  Sets the time step to be fixed.
 * \param CARD_VAR    Sets the time step to be variable.
 * \param CARD_FEULER Sets the solver to be forward Euler/explicit.
 * \param CARD_BEULER Not supported yet. Sets the solver to be implicit.
 * \param CARD_RK2    Sets the solver to use RK2 time integration.
 * \param CARD_RK4    Sets the solver to use RK4 time integration.
 * \param CARD_RKF45  Sets the solver to use RKF45 time integration.    
 * \param CARD_TOL    Sets the solver tolerance.
 * \param CARD_MAXIT  Not supported yet. Sets the max solver iterations.
 * \param CARD_NO     Corresponds to unrecognized cards.
 *
 * */

#ifndef _CARDS_H
#define _CARDS_H

typedef enum {
    // CARD_<name> = <cardvalue>,
    // General,
    CARD_PROB   =          65427,
    CARD_TIME   =         202483,
    CARD_SOLVER =     1035638010,
    // Problem,
    CARD_NVAR   =         793921,
    CARD_IC     =             80,
    CARD_MTYPE  =        7450104,
    CARD_KTYPE  =        7450102,
    // Matrix type,
    CARD_DENSE  =        7575267,
    CARD_SPARSE =      272891502,
    CARD_IDENTY =     1483712756,
    // Time,
    CARD_TSTART =     1177436107,
    CARD_TEND   =         156979,
    CARD_DT     =            687,
    CARD_FIXED  =        5255573,
    CARD_VAR    =          22053,
    // Solver,
    CARD_FEULER =     1035182741,
    CARD_BEULER =     1035182737,
    CARD_RK2    =          37961,
    CARD_RK4    =          40553,
    CARD_RKF45  =       55200905,
    CARD_TOL    =          14779,
    CARD_MAXIT  =       32315772,
    // Others,
    CARD_NO     =            517
} CARD;

#endif
