/*! \file extern.h
 * \brief Includes header files of external libraries.
 * */
#ifndef EXTERN_HEADER_H
#define EXTERN_HEADER_H

#ifdef D_GSL
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>
#endif /* #ifdef D_GSL */

#endif /* #ifndef EXTERN_HEADER_H */
