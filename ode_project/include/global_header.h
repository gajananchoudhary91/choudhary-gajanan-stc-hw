/*! \file global_header.h
 * \brief Includes all header files of this project.
 *
 * \details
 * This header file is included at the top of almost all program files of 
 * this project, so that no other header files have to be included.
 * It includes the following files:
 *   -# define.h: Standard C libraries and basic constants
 *   -# macro.h: Some useful macros
 *   -# cards.h: Cards for reading input file options
 *   -# extern.h: External library header files
 *   -# type.h: Data structure headers
 *   -# fnctn.h: Function definitions that use multiple data types
 *
 * */

#ifndef GLOBAL_HEADER_H
#define GLOBAL_HEADER_H

#include "define.h" /* Standard C libraries and basic #defines */
#include "macro.h"  /* Some useful macros */
#include "cards.h"  /* Cards for reading input file options */
#include "extern.h" /* External library header files */
#include "type.h"   /* #includes of data structure headers */
#include "fnctn.h"  /* Function definitions that use multiple data types */

#endif /* #ifndef GLOBAL_HEADER_H */
