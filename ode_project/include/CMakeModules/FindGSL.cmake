# Try to find gnu scientific library GSL
# See 
# http://www.gnu.org/software/gsl/  and 
# http://gnuwin32.sourceforge.net/packages/gsl.htm
#
# Based on a script of Felix Woelk and Jan Woetzel
# (www.mip.informatik.uni-kiel.de)
# 
# It defines the following variables:
#  GSL_FOUND - system has GSL lib
#  GSL_INCLUDE_DIRS - where to find headers 
#  GSL_LIBRARIES - full path to the libraries
#  GSL_LIBRARY_DIRS, the directory where the PLplot library is found.
 
#  CMAKE_GSL_CXX_FLAGS  = Unix compiler flags for GSL, essentially "`gsl-config --cxxflags`"
#  GSL_LINK_DIRECTORIES = link directories, useful for rpath on Unix
#  GSL_EXE_LINKER_FLAGS = rpath on Unix
 
set( GSL_FOUND OFF )
set( GSL_CBLAS_FOUND OFF )
 
# Windows, but not for Cygwin and MSys where gsl-config is available
if( WIN32 AND NOT CYGWIN AND NOT MSYS )
  # look for headers
  find_path( GSL_INCLUDE_DIR
    NAMES gsl/gsl_cdf.h gsl/gsl_randist.h
    )
  if( GSL_INCLUDE_DIR )
    # look for gsl library
    find_library( GSL_LIBRARY
      NAMES gsl 
    )  
    if( GSL_LIBRARY )
      set( GSL_INCLUDE_DIRS ${GSL_INCLUDE_DIR} )
      get_filename_component( GSL_LIBRARY_DIRS ${GSL_LIBRARY} PATH )
      set( GSL_FOUND ON )
    endif( GSL_LIBRARY )
 
    # look for gsl cblas library
    find_library( GSL_CBLAS_LIBRARY
        NAMES gslcblas 
      )
    if( GSL_CBLAS_LIBRARY )
      set( GSL_CBLAS_FOUND ON )
    endif( GSL_CBLAS_LIBRARY )
       
    set( GSL_LIBRARIES ${GSL_LIBRARY} ${GSL_CBLAS_LIBRARY} )
  endif( GSL_INCLUDE_DIR )
   
  mark_as_advanced(
    GSL_INCLUDE_DIR
    GSL_LIBRARY
    GSL_CBLAS_LIBRARY
  )
else( WIN32 AND NOT CYGWIN AND NOT MSYS )
  if( UNIX OR MSYS )
    find_program( GSL_CONFIG_EXECUTABLE gsl-config
      /usr/bin/
      /usr/local/bin
    )
     
    if( GSL_CONFIG_EXECUTABLE ) 
      set( GSL_FOUND ON )
       
      # run the gsl-config program to get cxxflags
      execute_process(
        COMMAND sh "${GSL_CONFIG_EXECUTABLE}" --cflags
        OUTPUT_VARIABLE GSL_CFLAGS
        RESULT_VARIABLE RET
        ERROR_QUIET
        )
      if( RET EQUAL 0 )
        string( STRIP "${GSL_CFLAGS}" GSL_CFLAGS )
        separate_arguments( GSL_CFLAGS )
 
        # parse definitions from cflags; drop -D* from CFLAGS
        string( REGEX MATCHALL "-D[^;]+"
          GSL_DEFINITIONS  "${GSL_CFLAGS}" )
        string( REGEX REPLACE "-D[^;]+;" ""
          GSL_CFLAGS "${GSL_CFLAGS}" )
 
        # parse include dirs from cflags; drop -I prefix
        string( REGEX MATCHALL "-I[^;]+"
          GSL_INCLUDE_DIRS "${GSL_CFLAGS}" )
        string( REPLACE "-I" ""
          GSL_INCLUDE_DIRS "${GSL_INCLUDE_DIRS}")
        string( REGEX REPLACE "-I[^;]+;" ""
          GSL_CFLAGS "${GSL_CFLAGS}")
 
        message("GSL_DEFINITIONS=${GSL_DEFINITIONS}")
        message("GSL_INCLUDE_DIRS=${GSL_INCLUDE_DIRS}")
        message("GSL_CFLAGS=${GSL_CFLAGS}")
      else( RET EQUAL 0 )
        set( GSL_FOUND FALSE )
      endif( RET EQUAL 0 )
 
      # run the gsl-config program to get the libs
      execute_process(
        COMMAND sh "${GSL_CONFIG_EXECUTABLE}" --libs
        OUTPUT_VARIABLE GSL_LIBRARIES
        RESULT_VARIABLE RET
        ERROR_QUIET
        )
      if( RET EQUAL 0 )
        string(STRIP "${GSL_LIBRARIES}" GSL_LIBRARIES )
        separate_arguments( GSL_LIBRARIES )
 
        # extract linkdirs (-L) for rpath (i.e., LINK_DIRECTORIES)
        string( REGEX MATCHALL "-L[^;]+"
          GSL_LIBRARY_DIRS "${GSL_LIBRARIES}" )
        string( REPLACE "-L" ""
          GSL_LIBRARY_DIRS "${GSL_LIBRARY_DIRS}" )
      else( RET EQUAL 0 )
        set( GSL_FOUND FALSE )
      endif( RET EQUAL 0 )
       
      MARK_AS_ADVANCED(
        GSL_CFLAGS
      )
      message( STATUS "Using GSL from ${GSL_PREFIX}" )
    else( GSL_CONFIG_EXECUTABLE )
      message( STATUS "FindGSL: gsl-config not found.")
    endif( GSL_CONFIG_EXECUTABLE )
  endif( UNIX OR MSYS )
endif( WIN32 AND NOT CYGWIN AND NOT MSYS )
 
if( GSL_FOUND )
  if( NOT GSL_FIND_QUIETLY )
    message( STATUS "FindGSL: Found both GSL headers and library" )
  endif( NOT GSL_FIND_QUIETLY )
else( GSL_FOUND )
  if( GSL_FIND_REQUIRED )
    message( FATAL_ERROR "FindGSL: Could not find GSL headers or library" )
  endif( GSL_FIND_REQUIRED )
endif( GSL_FOUND )
###### Distributed under the OSI-approved BSD 3-Clause License.  See accompanying
###### file Copyright.txt or https://cmake.org/licensing for details.
#####
######.rst:
###### FindGSL
###### --------
######
###### Find the native GSL includes and libraries.
######
###### The GNU Scientific Library (GSL) is a numerical library for C and C++
###### programmers. It is free software under the GNU General Public
###### License.
######
###### Imported Targets
###### ^^^^^^^^^^^^^^^^
######
###### If GSL is found, this module defines the following :prop_tgt:`IMPORTED`
###### targets::
######
######  GSL::gsl      - The main GSL library.
######  GSL::gslcblas - The CBLAS support library used by GSL.
######
###### Result Variables
###### ^^^^^^^^^^^^^^^^
######
###### This module will set the following variables in your project::
######
######  GSL_FOUND          - True if GSL found on the local system
######  GSL_INCLUDE_DIRS   - Location of GSL header files.
######  GSL_LIBRARIES      - The GSL libraries.
######  GSL_VERSION        - The version of the discovered GSL install.
######
###### Hints
###### ^^^^^
######
###### Set ``GSL_ROOT_DIR`` to a directory that contains a GSL installation.
######
###### This script expects to find libraries at ``$GSL_ROOT_DIR/lib`` and the GSL
###### headers at ``$GSL_ROOT_DIR/include/gsl``.  The library directory may
###### optionally provide Release and Debug folders. If available, the libraries
###### named ``gsld``, ``gslblasd`` or ``cblasd`` are recognized as debug libraries.
###### For Unix-like systems, this script will use ``$GSL_ROOT_DIR/bin/gsl-config``
###### (if found) to aid in the discovery of GSL.
######
###### Cache Variables
###### ^^^^^^^^^^^^^^^
######
###### This module may set the following variables depending on platform and type
###### of GSL installation discovered.  These variables may optionally be set to
###### help this module find the correct files::
######
######  GSL_CBLAS_LIBRARY       - Location of the GSL CBLAS library.
######  GSL_CBLAS_LIBRARY_DEBUG - Location of the debug GSL CBLAS library (if any).
######  GSL_CONFIG_EXECUTABLE   - Location of the ``gsl-config`` script (if any).
######  GSL_LIBRARY             - Location of the GSL library.
######  GSL_LIBRARY_DEBUG       - Location of the debug GSL library (if any).
######
#####
#####include(${CMAKE_CURRENT_LIST_DIR}/FindPackageHandleStandardArgs.cmake)
#####
######=============================================================================
###### If the user has provided ``GSL_ROOT_DIR``, use it!  Choose items found
###### at this location over system locations.
#####if( EXISTS "$ENV{GSL_ROOT_DIR}" )
#####  file( TO_CMAKE_PATH "$ENV{GSL_ROOT_DIR}" GSL_ROOT_DIR )
#####  set( GSL_ROOT_DIR "${GSL_ROOT_DIR}" CACHE PATH "Prefix for GSL installation." )
#####endif()
#####if( NOT EXISTS "${GSL_ROOT_DIR}" )
#####  set( GSL_USE_PKGCONFIG ON )
#####endif()
#####
######=============================================================================
###### As a first try, use the PkgConfig module.  This will work on many
###### *NIX systems.  See :module:`findpkgconfig`
###### This will return ``GSL_INCLUDEDIR`` and ``GSL_LIBDIR`` used below.
#####if( GSL_USE_PKGCONFIG )
#####  find_package(PkgConfig)
#####  pkg_check_modules( GSL QUIET gsl )
#####
#####  if( EXISTS "${GSL_INCLUDEDIR}" )
#####    get_filename_component( GSL_ROOT_DIR "${GSL_INCLUDEDIR}" DIRECTORY CACHE)
#####  endif()
#####endif()
#####
######=============================================================================
###### Set GSL_INCLUDE_DIRS and GSL_LIBRARIES. If we skipped the PkgConfig step, try
###### to find the libraries at $GSL_ROOT_DIR (if provided) or in standard system
###### locations.  These find_library and find_path calls will prefer custom
###### locations over standard locations (HINTS).  If the requested file is not found
###### at the HINTS location, standard system locations will be still be searched
###### (/usr/lib64 (Redhat), lib/i386-linux-gnu (Debian)).
#####
#####find_path( GSL_INCLUDE_DIR
#####  NAMES gsl/gsl_sf.h
#####  HINTS ${GSL_ROOT_DIR}/include ${GSL_INCLUDEDIR}
#####)
#####find_library( GSL_LIBRARY
#####  NAMES gsl
#####  HINTS ${GSL_ROOT_DIR}/lib ${GSL_LIBDIR}
#####  PATH_SUFFIXES Release Debug
#####)
#####find_library( GSL_CBLAS_LIBRARY
#####  NAMES gslcblas cblas
#####  HINTS ${GSL_ROOT_DIR}/lib ${GSL_LIBDIR}
#####  PATH_SUFFIXES Release Debug
#####)
###### Do we also have debug versions?
#####find_library( GSL_LIBRARY_DEBUG
#####  NAMES gsld gsl
#####  HINTS ${GSL_ROOT_DIR}/lib ${GSL_LIBDIR}
#####  PATH_SUFFIXES Debug
#####)
#####find_library( GSL_CBLAS_LIBRARY_DEBUG
#####  NAMES gslcblasd cblasd gslcblas cblas
#####  HINTS ${GSL_ROOT_DIR}/lib ${GSL_LIBDIR}
#####  PATH_SUFFIXES Debug
#####)
#####set( GSL_INCLUDE_DIRS ${GSL_INCLUDE_DIR} )
#####set( GSL_LIBRARIES ${GSL_LIBRARY} ${GSL_CBLAS_LIBRARY} )
#####
###### If we didn't use PkgConfig, try to find the version via gsl-config or by
###### reading gsl_version.h.
#####if( NOT GSL_VERSION )
#####  # 1. If gsl-config exists, query for the version.
#####  find_program( GSL_CONFIG_EXECUTABLE
#####    NAMES gsl-config
#####    HINTS "${GSL_ROOT_DIR}/bin"
#####    )
#####  if( EXISTS "${GSL_CONFIG_EXECUTABLE}" )
#####    execute_process(
#####      COMMAND "${GSL_CONFIG_EXECUTABLE}" --version
#####      OUTPUT_VARIABLE GSL_VERSION
#####      OUTPUT_STRIP_TRAILING_WHITESPACE )
#####  endif()
#####
#####  # 2. If gsl-config is not available, try looking in gsl/gsl_version.h
#####  if( NOT GSL_VERSION AND EXISTS "${GSL_INCLUDE_DIRS}/gsl/gsl_version.h" )
#####    file( STRINGS "${GSL_INCLUDE_DIRS}/gsl/gsl_version.h" gsl_version_h_contents REGEX "define GSL_VERSION" )
#####    string( REGEX REPLACE ".*([0-9]\\.[0-9][0-9]?).*" "\\1" GSL_VERSION ${gsl_version_h_contents} )
#####  endif()
#####
#####  # might also try scraping the directory name for a regex match "gsl-X.X"
#####endif()
#####
######=============================================================================
###### handle the QUIETLY and REQUIRED arguments and set GSL_FOUND to TRUE if all
###### listed variables are TRUE
#####find_package_handle_standard_args( GSL
#####  FOUND_VAR
#####    GSL_FOUND
#####  REQUIRED_VARS
#####    GSL_INCLUDE_DIR
#####    GSL_LIBRARY
#####    GSL_CBLAS_LIBRARY
#####  VERSION_VAR
#####    GSL_VERSION
#####    )
#####
#####mark_as_advanced( GSL_ROOT_DIR GSL_VERSION GSL_LIBRARY GSL_INCLUDE_DIR
#####  GSL_CBLAS_LIBRARY GSL_LIBRARY_DEBUG GSL_CBLAS_LIBRARY_DEBUG
#####  GSL_USE_PKGCONFIG GSL_CONFIG )
#####
######=============================================================================
###### Register imported libraries:
###### 1. If we can find a Windows .dll file (or if we can find both Debug and
######    Release libraries), we will set appropriate target properties for these.
###### 2. However, for most systems, we will only register the import location and
######    include directory.
#####
###### Look for dlls, or Release and Debug libraries.
#####if(WIN32)
#####  string( REPLACE ".lib" ".dll" GSL_LIBRARY_DLL       "${GSL_LIBRARY}" )
#####  string( REPLACE ".lib" ".dll" GSL_CBLAS_LIBRARY_DLL "${GSL_CBLAS_LIBRARY}" )
#####  string( REPLACE ".lib" ".dll" GSL_LIBRARY_DEBUG_DLL "${GSL_LIBRARY_DEBUG}" )
#####  string( REPLACE ".lib" ".dll" GSL_CBLAS_LIBRARY_DEBUG_DLL "${GSL_CBLAS_LIBRARY_DEBUG}" )
#####endif()
#####
#####if( GSL_FOUND AND NOT TARGET GSL::gsl )
#####  if( EXISTS "${GSL_LIBRARY_DLL}" AND EXISTS "${GSL_CBLAS_LIBRARY_DLL}")
#####
#####    # Windows systems with dll libraries.
#####    add_library( GSL::gsl      SHARED IMPORTED )
#####    add_library( GSL::gslcblas SHARED IMPORTED )
#####
#####    # Windows with dlls, but only Release libraries.
#####    set_target_properties( GSL::gslcblas PROPERTIES
#####      IMPORTED_LOCATION_RELEASE         "${GSL_CBLAS_LIBRARY_DLL}"
#####      IMPORTED_IMPLIB                   "${GSL_CBLAS_LIBRARY}"
#####      INTERFACE_INCLUDE_DIRECTORIES     "${GSL_INCLUDE_DIRS}"
#####      IMPORTED_CONFIGURATIONS           Release
#####      IMPORTED_LINK_INTERFACE_LANGUAGES "C" )
#####    set_target_properties( GSL::gsl PROPERTIES
#####      IMPORTED_LOCATION_RELEASE         "${GSL_LIBRARY_DLL}"
#####      IMPORTED_IMPLIB                   "${GSL_LIBRARY}"
#####      INTERFACE_INCLUDE_DIRECTORIES     "${GSL_INCLUDE_DIRS}"
#####      IMPORTED_CONFIGURATIONS           Release
#####      IMPORTED_LINK_INTERFACE_LANGUAGES "C"
#####      INTERFACE_LINK_LIBRARIES          GSL::gslcblas )
#####
#####    # If we have both Debug and Release libraries
#####    if( EXISTS "${GSL_LIBRARY_DEBUG_DLL}" AND EXISTS "${GSL_CBLAS_LIBRARY_DEBUG_DLL}")
#####      set_property( TARGET GSL::gslcblas APPEND PROPERTY IMPORTED_CONFIGURATIONS Debug )
#####      set_target_properties( GSL::gslcblas PROPERTIES
#####        IMPORTED_LOCATION_DEBUG           "${GSL_CBLAS_LIBRARY_DEBUG_DLL}"
#####        IMPORTED_IMPLIB_DEBUG             "${GSL_CBLAS_LIBRARY_DEBUG}" )
#####      set_property( TARGET GSL::gsl APPEND PROPERTY IMPORTED_CONFIGURATIONS Debug )
#####      set_target_properties( GSL::gsl PROPERTIES
#####        IMPORTED_LOCATION_DEBUG           "${GSL_LIBRARY_DEBUG_DLL}"
#####        IMPORTED_IMPLIB_DEBUG             "${GSL_LIBRARY_DEBUG}" )
#####    endif()
#####
#####  else()
#####
#####    # For all other environments (ones without dll libraries), create
#####    # the imported library targets.
#####    add_library( GSL::gsl      UNKNOWN IMPORTED )
#####    add_library( GSL::gslcblas UNKNOWN IMPORTED )
#####    set_target_properties( GSL::gslcblas PROPERTIES
#####      IMPORTED_LOCATION                 "${GSL_CBLAS_LIBRARY}"
#####      INTERFACE_INCLUDE_DIRECTORIES     "${GSL_INCLUDE_DIRS}"
#####      IMPORTED_LINK_INTERFACE_LANGUAGES "C" )
#####    set_target_properties( GSL::gsl PROPERTIES
#####      IMPORTED_LOCATION                 "${GSL_LIBRARY}"
#####      INTERFACE_INCLUDE_DIRECTORIES     "${GSL_INCLUDE_DIRS}"
#####      IMPORTED_LINK_INTERFACE_LANGUAGES "C"
#####      INTERFACE_LINK_LIBRARIES          GSL::gslcblas )
#####  endif()
#####endif()
