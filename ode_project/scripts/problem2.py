from __future__ import unicode_literals
import matplotlib
matplotlib.use('Agg') # Must be before importing matplotlib.pyplot or pylab!
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.unicode'] = True
import matplotlib.pyplot as plt
import numpy as np

for timestepper in ['FEULER', 'RK2', 'RK4', 'RKF45']:
    
    time, x, y, z, der, yder, zder = np.loadtxt('problem2_'+timestepper+'-sol.dat', delimiter=',', unpack=True, skiprows=1)
    a = [x, y, z]
    i=0
    for var in ['x', 'y', 'z']:
        plt.ioff()  # Use non-interactive mode.
        plt.plot(time, a[i][:], label=timestepper+r' sol ($'+var+'$)')
        plt.xlabel(r'Time($t$)')
        plt.ylabel(r'Solution ($'+var+'$)')
        plt.title(r'Solution $'+var+'$ using '+timestepper+' time stepping')
        plt.axis('equal')
        plt.grid('on')
        plt.legend(loc='upper right')
        
        plt.savefig('problem2_'+timestepper+'_'+var+'_solution_plot.pdf')
        plt.close()
        i=i+1
