from __future__ import unicode_literals
import matplotlib
matplotlib.use('Agg') # Must be before importing matplotlib.pyplot or pylab!
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.unicode'] = True
import matplotlib.pyplot as plt
import numpy as np

for timestepper in ['FEULER', 'RK2', 'RK4', 'RKF45']:
    
    time, y, yder = np.loadtxt('unit_test_sincos_'+timestepper+'-sol.dat', delimiter=',', unpack=True, skiprows=1)
    time, ana_y, ana_yder = np.loadtxt('unit_test_sincos_'+timestepper+'-analytical-sol.dat', delimiter=',', unpack=True, skiprows=1)
    time, err_y = np.loadtxt('unit_test_sincos_'+timestepper+'-error-sol.dat', delimiter=',', unpack=True, skiprows=1)
    
    plt.ioff()  # Use non-interactive mode.
    f, arr = plt.subplots(3, sharex=True)
    plt.suptitle(r'Solution and derivative plot for '+timestepper+' time stepping')
    arr[0].plot(time,     y, label=timestepper+r' sol ($y$)')
    arr[0].plot(time, ana_y, label=r'Analytical sol')
    #arr[0].set_xlabel(r'Time($t$)')
    arr[0].set_ylabel(r'Solution ($y(t)$)')
    #arr[0].set_title(r'Solution $y$.')
    #plt.axis('equal')
    arr[0].grid('on')
    arr[0].legend(loc='upper right')
    
    arr[1].plot(time,     yder, label=timestepper+r' sol ($\frac{dy}{dt}$)')
    arr[1].plot(time, ana_yder, label=r'Analytical sol')
    #arr[1].set_xlabel(r'Time($t$)')
    arr[1].set_ylabel(r'Solution ($\frac{dy}{dt}$)')
    #arr[1].set_title(r'Solution $\frac{dy}{dt}$.')
    #plt.axis('equal')
    arr[1].grid('on')
    arr[1].legend(loc='upper right')
    
    arr[2].plot(time, err_y, label=r'$||err||_{l^2}$')
    arr[2].set_xlabel(r'Time($t$)')
    arr[2].set_ylabel(r'$||err||_{l^2}$)')
    #arr[2].set_title(r'Error $||err||_{l^2}$.')
    arr[2].grid('on')
    arr[2].legend(loc='upper right')

    f.savefig(timestepper+'_solution_plot.pdf')
    plt.close()
