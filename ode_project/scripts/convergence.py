from __future__ import unicode_literals
import matplotlib
matplotlib.use('Agg') # Must be before importing matplotlib.pyplot or pylab!
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.unicode'] = True
import matplotlib.pyplot as plt
import numpy as np

for timestepper in ['FEULER', 'RK2', 'RK4', 'RKF45']:
    
    dt, lastl2, cuml2, intL2, logdt, loglastl2, logcuml2, logintL2 = np.loadtxt('unit_test_convergence_rate_'+timestepper+'-errors.dat', delimiter=',', unpack=True, skiprows=1)
    
    plt.ioff()  # Use non-interactive mode.
    plt.figure();
    plt.plot(dt, lastl2, label=r'$||err||_{l^2}$ last time', marker='o')
    plt.plot(dt,  cuml2, label=r'$||err||_{l^2}$ cumulative', marker='*')
    plt.plot(dt,  intL2, label=r'$||err||_{L^2}$ integrated', marker='+')
    plt.loglog()
    plt.xlabel(r'\textbf{log(h)}')
    plt.ylabel(r'\textbf{log(Error)}')
    plt.title('Convergence analysis of '+timestepper+' time stepping.')
    plt.axis('equal')
    plt.xlim([0.0001, 10])
    plt.grid('on')
    plt.legend(loc='upper right')
    
    plt.savefig(timestepper+'_convergence_plot.pdf')
    plt.close()
