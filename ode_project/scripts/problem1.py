from __future__ import unicode_literals
import matplotlib
matplotlib.use('Agg') # Must be before importing matplotlib.pyplot or pylab!
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.unicode'] = True
import matplotlib.pyplot as plt
import numpy as np

for timestepper in ['FEULER', 'RK2', 'RK4', 'RKF45']:
    
    time, y, yder = np.loadtxt('problem1_'+timestepper+'-sol.dat', delimiter=',', unpack=True, skiprows=1)
    
    plt.ioff()  # Use non-interactive mode.
    plt.plot(time,    y, label=timestepper+r' sol ($y$)')
    plt.plot(time, yder, label=timestepper+r' sol ($\frac{dy}{dt}$)')
    plt.xlabel(r'Time($t$)')
    plt.ylabel(r'Solution ($y(t)$)')
    plt.title(r'Solution $y$ using '+timestepper+' time stepping')
    plt.axis('equal')
    plt.grid('on')
    plt.legend(loc='upper right')
    
    plt.savefig('problem1_'+timestepper+'_solution_plot.pdf')
    plt.close()
