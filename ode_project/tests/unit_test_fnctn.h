/* \file unit_test_fnctn.h
 * \brief Contains function declarations of only the unit testing programs.
 * */

/* unit_test_tools_double */
int test_tools_double();
int test_tl_dbl_init_array(double *, int);
int test_tl_dbl_copy_array(double *, int, double *);
int test_tl_dbl_mat_array_prod();

/* unit_test_sincos */
int unit_test_sincos_initialize(MODEL_STRUCT **, char **);
int unit_test_sincos_run(MODEL_STRUCT *, 
                         void (*fnctn_ana_sol)(const double, 
                                               const void *, 
                                               double *
                              )
                        );
/* unit_test_convergence */
int unit_test_convergence_initialize(MODEL_STRUCT **, char *, double);
int unit_test_convergence_run(MODEL_STRUCT *, 
                              void (*fnctn_ana_sol)(const double, 
                                                    const void *, 
                                                    double *
                                   )
                             );
int analyze_errors(const double *, const double *, const double *, int, int);

/* Other tools */
double calc_l2_error(const double *, const double *, const int);
void sincos_analytical_solution(const double t, const void *params, 
                                double *sol);
