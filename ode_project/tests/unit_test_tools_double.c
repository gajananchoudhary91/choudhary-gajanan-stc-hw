/*! \file unit_test_tools_double.c
 * \brief Tests the double-related functions in the tools folder.
 * \details
 * This unit test is for testing the general double-array-related functions
 * in the tools/ folder. We test
 *     -# tl_dbl_init_array()
 *     -# tl_dbl_copy_array()
 *     -# tl_dbl_mat_array_prod()
 * */

#include "global_header.h"
#include "unit_test_fnctn.h"

#define TOL  1.0E-12 /* Tolerance for checking matrix vector product. */

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
int main(){
    int ierr = FAILURE;
    ierr = test_tools_double();
    PRINT_MANY_STARS();
    PRINT_TEST_OUTCOME(ierr, "Unit testing of general tools.");
    if (SUCCESS == ierr) return EXIT_SUCCESS; else return EXIT_FAILURE;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
int test_tools_double(){
    int ierr=FAILURE, temp_ierr=FAILURE;
    /* Random arrays: */
    double b[7] = {-0.155879, 5.1, 7.52e-125, -125081592.0, 789.2, 0.02, 1.0};
    double a[7] = {2130.175, 1852.92, -0.0701, 0.125606, 1.4E+200, -5.1E-300};

    PRINT_FEW_STARS();
    ierr = test_tl_dbl_init_array(b, 7);
    PRINT_TEST_OUTCOME(ierr, "Testing array initialization (to 0.0)");
    //if (SUCCESS==ierr){
    //    printf(COLOR_GREEN "SUCCESS" COLOR_RESET
    //           ": Testing array initialization (to zero)\n");
    //}
    //else{
    //    printf(COLOR_RED "FAILURE" COLOR_RESET
    //           ": Testing array initialization (to zero)\n");
    //}
    
    temp_ierr = test_tl_dbl_copy_array(a, 7, b);
    ierr += temp_ierr;
    PRINT_TEST_OUTCOME(temp_ierr, "Testing array copying.");
    
    temp_ierr = test_tl_dbl_mat_array_prod();
    ierr += temp_ierr;
    PRINT_TEST_OUTCOME(temp_ierr, "Testing matrix-array product.");
    
    if (3*SUCCESS==ierr) return SUCCESS;
    else return FAILURE;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
int test_tl_dbl_init_array(double *a, int asize){
    int i=0;
    tl_dbl_init_array(a, asize);
#ifdef D_DEBUG
    printf("****************\n");
    printf("Testing array initialization (to zero).\n");
    printf("    Check if the following array is exactly zero\n");
    for (i=0; i<asize; i++) printf("    % 24.15e\n", a[i]);
#endif
    for (i=0; i<asize; i++){
       if(0.0 != a[i]){ /* Should be EXACTLY zero for success */
           return FAILURE;
       }
    }
    return SUCCESS;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
int test_tl_dbl_copy_array(double *aIn, int size, double *bOut){
    int i=0;
    tl_dbl_copy_array(aIn, size, bOut);
#ifdef D_DEBUG
    printf("****************\n");
    printf("Testing array copying.\n");
    printf("             Correct copy (aIn) |"
           "           Copied values (bOut) |"
           "                  abs(bOut-aIn)\n");
    for (i=0; i<size; i++) printf("       % 24.16e |       % 24.16e |"
                                  "       % 24.16e\n", aIn[i], bOut[i], 
                                  fabs(bOut[i]-aIn[i]));
#endif
    for (i=0; i<size; i++){
       if(bOut[i] != aIn[i]){
           return FAILURE;
       }
    }
    return SUCCESS;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
int test_tl_dbl_mat_array_prod(){
    int i=0;
    /* correct_outVec is hand-calculated output of (mat * inVec) */
    // double mat[3][2]={0.279, 0.300, -4.100, 2.761, 0.0039, -5112.2};
    double **mat = (double **) malloc(sizeof(double *)*3);
    mat[0] = (double *) malloc(sizeof(double)*6);
    mat[1] = mat[0]+2;
    mat[2] = mat[1]+2;
    mat[0][0] =     0.279;
    mat[0][1] =     0.300;
    mat[1][0] =    -4.100;
    mat[1][1] =     2.761;
    mat[2][0] =     0.0039;
    mat[2][1] = -5112.2;

    double inVec[2]={-1.100, 0.200};
    double correct_outVec[3]={-0.2469, 5.0622, -1022.44429};
    
    double outVec[3]={-0.2,251253.2,-0.00008491230}; /* Random numbers */
    tl_dbl_mat_array_prod(mat, inVec, 3, 2, outVec);
    
#ifdef D_DEBUG
    printf("****************\n");
    printf("Testing matrix-vector product.\n");
    printf("    Correct product:  {% 24.16e, % 24.16e, % 24.16e}\n", 
           correct_outVec[0], correct_outVec[1], correct_outVec[2]);
    printf("    Product obtained: {% 24.16e, % 24.16e, % 24.16e}\n", 
           outVec[0], outVec[1], outVec[2]);
    printf("    Abs(difference):  {% 24.16e, % 24.16e, % 24.16e}\n", 
           fabs(outVec[0] - correct_outVec[0]),
           fabs(outVec[1] - correct_outVec[1]),
           fabs(outVec[2] - correct_outVec[2]));
#endif
    
    for (i=0; i<3; i++){
       if(fabs(correct_outVec[i] - outVec[i])>TOL){
           return FAILURE;
       }
    }
    free(mat[0]);
    free(mat);
    return SUCCESS;
}
