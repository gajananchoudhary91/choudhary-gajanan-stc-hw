/*! \file unit_test_sincos.c
 * \brief This is the unit test for FEULER/RK2/RK4/RKF45 time stepping.
 * 
 * \details
 * This unit test solves the followed ODE:
 *       y''(t) = -(k^2)*y(t), t>0
 *         y(0) = y_0,
 *        y'(0) = yder_0.
 *
 * The analytical solution to this ODE is:
 *         y(t) = (yder_0/k)*sin(k*t) + (y_0)*cos(k*t)
 * 
 * To solve this using this project, we have to reformulate the problem in 
 * two variables as follows:
 *          y1' =       y2(t), t>0
 * (y1''=)  y2' = -(k^2)y1(t), t>0
 *        y1(0) = y_0
 *        y2(0) = yder_0
 * 
 * The analytical solution to this problem is:
 *        y1(t) = ( yder_0/k ) * sin(k*t) + (  y_0   )*cos(k*t)
 *        y2(t) = ( yder_0   ) * cos(k*t) + ( -y_0*k )*sin(k*t)
 * 
 * */
/****************************************************************************/
#include "global_header.h"
#include "unit_test_fnctn.h"

#define  MODEL_NAME "unit_test_sincos"
/* Set the 'k' value. MUST be greater than 0.0. Okay to change. */
#define  KVAL    6.2831853071795864769252867665590 /* (2*PI) */
/* Set the two initial conditions. Okay to change. */
#define  Y_0     0.0
#define  YDER_0    6.2831853071795864769252867665590 /* (2*PI) */
//#define  YDER_0  0.1591549430918953357688837633725 /* 1/(2*PI) */
/* Set the time parameters. Okay to change. */
#define T_END    3.0

/* DO NOT CHANGE THE FOLLOWING #DEFINES: */
#define NVAR     2
#define ZERO     0.0
#define ONE      1.0
#define L2_TOL       1.0E-00 /* Last time step l2 error tolerance */
#define RK_SOLV_TOL  1.0E-03

static time_t time0, time1; /* For total simulation time calculation */
static double l2err=0.0;    /* l2 error of solution at a single time step. */
static double error=0.0;    /* Cumulative l2 error of the solution. */
static int DEBUG=OFF;

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
int main(int argc, char *argv[]){
    assert(NVAR == 2);
    if (argc < 3) {
        printf("\nError: No command line arguments supplied!\n");
        printf("Please enter the time integration method (FEULER/RK2/RK4/"
               "RKF45) followed by the time step (dt, in seconds)"
               " as a command line argument...\n");
        printf("Terminating program run.\n\n");
        exit(EXIT_FAILURE);
    }
    else if (argc > 3) {
        printf("\nError: Too many command line arguments supplied!\n");
        printf("Please enter the time integration method (FEULER/RK2/RK4/"
               "RKF45) followed by the time step (dt, in seconds)"
               " as a command line argument...\n");
        printf("Terminating program run.\n\n");
        exit(EXIT_FAILURE);
    }
    else {
        printf("\n\n"); PRINT_MANY_STARS();
        printf("Starting program run\n");
        PRINT_MANY_STARS();
    }
    int ierr=0;              /* Error tracking variables. */
    double total_time=0.0;   /* Total run time of the code. */
    MODEL_STRUCT *model;     /* The model data structure declaration. */
    
    /************************************************************************/
    time(&time0);

    /************************************************************************/
    /* Allocate memory, initialize model, read input, and set up the run. */
    ierr = unit_test_sincos_initialize(&model, argv);
    CHECKSUCCESS(ierr);

    /************************************************************************/
    /* Run the model simulation. */
    ierr = unit_test_sincos_run(model, sincos_analytical_solution);
    CHECKSUCCESS(ierr);
    
    /************************************************************************/
    /* Clean up after a successful run. */
    ierr = model_free(&model);
    CHECKSUCCESS(ierr);
    
    /************************************************************************/
    /* Print timing information. */
    time(&time1);
    total_time = difftime(time1,time0);
    PRINT_FEW_STARS();
    printf("    Total time      = % .6fs\n", total_time);
    PRINT_FEW_STARS();
    
    /************************************************************************/
    printf("The last time step l2 error is   : % 24.16e\n", l2err);
    printf("The time-cumulative l2 error is  : % 24.16e\n", error);
    if (l2err>L2_TOL) {
        printf(COLOR_RED "Failed to satisfy (last time) tolerance of % 24.16e"
               COLOR_RESET "\n", L2_TOL);
        printf(COLOR_RED "Program exited with FAILURE" COLOR_RESET "\n");
        PRINT_MANY_STARS(); printf("\n\n");
        return EXIT_FAILURE;
    }
    else{
        printf(COLOR_GREEN "Satisfied (last time) tolerance of % 24.16e"
               COLOR_RESET "\n", L2_TOL);
        printf(COLOR_GREEN "Program exited with SUCCESS" COLOR_RESET "\n");
        PRINT_MANY_STARS(); printf("\n\n");
        return EXIT_SUCCESS;
    }
    return EXIT_SUCCESS;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/*! \fn int unit_test_sincos_initialize(MODEL_STRUCT **, char *)
 * \brief Initializes the unit test model.
 * \param model_ptr    Pointer to the address of the model.
 * \param argv         Command line argument of [1]solver type, [2]Time step.
 * \return SUCCESS     If model initialization was successfu.l
 * */
int unit_test_sincos_initialize(MODEL_STRUCT **model_ptr, 
                                char *argv[]){
    int ierr=FAILURE;
    int solver=0;
    double dt=0.0;
    char filename[MAXLINE+30];
    MODEL_STRUCT *model=NULL;

#ifdef D_DEBUG
    if (DEBUG) printf("Parsing command line argument \"%s\"\n", argv[1]);
#endif
    solver = parse_card(&argv[1]);
    switch (solver) {
        case CARD_FEULER:
            sprintf(filename, "%s_FEULER", MODEL_NAME);
            printf("Forward Euler selected for time stepping.\n");
            break;
        case CARD_RK2:
            sprintf(filename, "%s_RK2", MODEL_NAME);
            printf("RK2 selected for time stepping.\n");
            break;
        case CARD_RK4:
            sprintf(filename, "%s_RK4", MODEL_NAME);
            printf("RK4 selected for time stepping.\n");
            break;
        case CARD_RKF45:
            sprintf(filename, "%s_RKF45", MODEL_NAME);
            printf("RKF45 selected for time stepping.\n");
            break;
        default:
            throw_error("Only accepted command line args: "
                        "FEULER/RK2/RK4/RKF45");
            break;
    }
    dt = read_dbl_field(&(argv[2]), &ierr);
    if (READ_SUCCESS != ierr) throw_error("Error reading time step (double)");
    if (dt<SMALL6) throw_error("User-supplied time step too small/negative.");
    
    ierr = model_alloc_init(model_ptr, NVAR, filename);
    model = *model_ptr;
    if (FAILURE == ierr){
        printf("Failed somewhere in function model_alloc().");
        return FAILURE;
    }

    /* Define K. */
    model->K[0][0] =  ZERO;
    model->K[0][1] =  ONE;
    model->K[1][0] = -KVAL*KVAL;
    model->K[1][1] =  ZERO;
    /* Define initial conditions. */
    model->sol_old[0] = Y_0;
    model->sol_old[1] = YDER_0;
    /* Set time information. */
    model->tStart = ZERO;
    model->tEnd   = T_END;
    model->dt     = dt;
    model->max_dt = model->dt;
    model->t_prev = model->tStart;
    /* Set flags */
    model->flag.ADAPT_TIME=NO;
    model->flag.SOLVER_TYPE=solver;
    /* Set tolerance. */
    model->tol    = RK_SOLV_TOL;


    if  (  CARD_RK2   == model->flag.SOLVER_TYPE
        || CARD_RK4   == model->flag.SOLVER_TYPE
        || CARD_RKF45 == model->flag.SOLVER_TYPE ){
#ifdef D_GSL
        ierr = gsl_alloc_init(*model_ptr);
        if (SUCCESS != ierr) {
             printf("Error setting up the GSL system and driver.\n");
             return FAILURE;
        }
#else
        printf("Error. Code MUST be compiled with GSL to use RK methods.\n");
        model_free(&model);
        return FAILURE;
#endif
    }

    /* Open output file for writing. */
    write_initial_output(model->t_prev, model->sol_old,
                         model->nvar, model->model_name);
    sprintf(filename,"%s-analytical", model->model_name);
    write_initial_output(model->t_prev, model->sol_old, 
                         model->nvar, filename);
    sprintf(filename,"%s-error",model->model_name);
    double tempdouble = ZERO;
    write_initial_output(model->t_prev, &tempdouble, 1, filename);

    return SUCCESS;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/*! \fn int unit_test_sincos_run(MODEL_STRUCT *,
 *                               char *,
 *                             void (*)(const double, const void *, double *))
 * \brief Function that time-steps, and compares with the anlaytical solution.
 * \param model            Pointer to the model.
 * \param fnctn_ana_sol    Pointer to funtion that calculates analytical sol.
 * \return SUCCESS         If model simulation was successful.
 * */
int unit_test_sincos_run(MODEL_STRUCT *model,
                         void (*fnctn_ana_sol)(const double t,
                                               const void *params,
                                               double *sol
                                              )
                        ) {
    int ierr=FAILURE;
    double *analytical_sol = (double *)malloc(sizeof(double)*model->nvar);
    char filename[MAXLINE+30];

#ifdef D_DEBUG
    PRINT_FEW_STARS();
    printf("%s running\n", model->model_name);
    if (DEBUG){
        printf("**************\n");
        printf("Starting at time t_prev = %14.6e\n", model->t_prev);
    }
#endif

    model->t_curr = model->t_prev + model->dt;

    do {
#ifdef D_DEBUG
        if (DEBUG){
            printf("**************\n");
            printf("Solving for time t_curr = %14.6e\n", model->t_curr);
        }
#endif

        /* Solve the time step */
        ierr = solve_main(model);
        if (SUCCESS != ierr) return ierr;

        (*fnctn_ana_sol)(model->t_curr, model, analytical_sol);
        /* Write output */
        write_main(model->t_curr, model->sol, model->nvar, model->model_name);
        
        /* Write analytical solution */
        sprintf(filename,"%s-analytical", model->model_name);
        write_main(model->t_curr, analytical_sol, model->nvar, filename);

        /* Write l2 error */
        sprintf(filename,"%s-error", model->model_name);
        l2err = calc_l2_error(analytical_sol, model->sol, model->nvar);
        write_main(model->t_curr, &l2err, 1, filename);

        error+=l2err*l2err;

    }while (model_check_update_time(model)!=YES);
    free(analytical_sol);

    error = sqrt(error);

    return ierr;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
double calc_l2_error(const double *anasol, const double *sol, const int nvar){
    double l2_error = 0.0;
    int i=0;
    for(i=0; i<nvar; i++) l2_error += ( (anasol[i]-sol[i])
                                      * (anasol[i]-sol[i]) );
    return sqrt(l2_error);
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/*! \fn void sincos_analytical_solution(const double, const void *, double *)
 * \brief Function that gives the sin-cos anlaytical solution for y''=-k^2*y.
 * \param t          Time
 * \param params     Generic pointer to struct via type-casting
 * \param ana_sol    Array storing the analytical solution (output)
 * \details
 * This function is gives the sinuosoidal analytical solution to the problem:
 *          y1' =       y2(t), t>0
 * (y1''=)  y2' = -(k^2)y1(t), t>0
 *        y1(0) = y_0
 *        y2(0) = yder_0
 * 
 * The analytical solution to this problem is:
 *        y1(t) = ( yder_0/k ) * sin(k*t) + (  y_0   )*cos(k*t)
 *        y2(t) = ( yder_0   ) * cos(k*t) + ( -y_0*k )*sin(k*t)
 * */
void sincos_analytical_solution(const double t, const void *params, 
                                double *ana_sol){
    MODEL_STRUCT *mod = (MODEL_STRUCT *) params;
    assert(mod->nvar == 2);
    
    /* Yes I know this is a float comparison. But this is intentional.
     * in order to prevent myself from making foolish mistakes if I decide
     * to change this test case later. The idea is that only parameter 
     * mod->K[1][0] should be allowed to change, and that too it should be 
     * strictly negative, for now.
     * */
#ifdef D_DEBUG
    if (  mod->K[0][0] != ZERO && mod->K[1][1] != ZERO 
       && mod->K[0][1] != ONE  && mod->K[1][0] >= ZERO /*Note the >= */){
        throw_error("You have broken the test case. Check your K matrix.");
    }
#endif
    ana_sol[0] = ( YDER_0/KVAL ) * sin(KVAL*t) + (  Y_0      ) * cos(KVAL*t);
    ana_sol[1] = ( YDER_0      ) * cos(KVAL*t) + ( -Y_0*KVAL ) * sin(KVAL*t);
}
