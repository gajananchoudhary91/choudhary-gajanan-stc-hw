cmake_minimum_required(VERSION 2.8.10)
# Ctest unit testing
option(UNIT_TEST "Unit Testing" OFF)
if(UNIT_TEST)
    enable_testing()
endif(UNIT_TEST)


project(ode C)

set(CMAKE_C_COMPILER gcc)

# Include Directory, needed for main/ode.c
include_directories("${PROJECT_SOURCE_DIR}/include" "${PROJECT_SOURCE_DIR}/structs")

# Specify binary and library output directories
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")

# Specify modules directory for finding external libraries. "Find*.cmake"
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${PROJECT_SOURCE_DIR}/include/CMakeModules")

# Debug options
option(DEBUG "Debug options" ON)
if(DEBUG)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -g -Wall -Wextra -Werror -pedantic -O0")
    add_definitions(-DD_DEBUG)
endif(DEBUG)

# GSL library
option(USE_GSL "GSL library" ON)
if(USE_GSL)
    #include("${CMAKE_MODULE_PATH}/FindGSL.cmake")
    find_package(GSL REQUIRED)
    if (GSL_FOUND)
        set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -L${GSL_LIBRARY_DIRS} -lgsl -lgslcblas")
        include_directories(${GSL_INCLUDE_DIRS} ${GSL_LIBRARY_DIRS})
        #add_definitions(-lgsl -lgslcblas)
        add_definitions(-DD_GSL)
    else (GSL_FOUND)
        message(FATAL_ERROR "Error. GSL library not found.")
    endif (GSL_FOUND)
else(USE_GSL)
    if(UNIT_TEST)
        message(FATAL_ERROR "Error. Cannot use testing mode without adding GSL Library. Flag: -USE_GSL=ON")
    endif(UNIT_TEST)
endif(USE_GSL)


# Gprof profiling
option(PROFILING "Profiling" OFF)
if(PROFILING)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -pg")
endif(PROFILING)


# Gcov coverage
option(COVERAGE "Coverage" OFF)
if(COVERAGE)
    if (CMAKE_COMPILER_IS_GNUCC)
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS}  -fprofile-arcs -ftest-coverage --coverage -g -O0 ")
        message("Coverage option selected.")
    else (CMAKE_COMPILER_IS_GNUCC)
        message(FATAL_ERROR "Error. Coverage option requires 'gcc' compiler.")
    endif (CMAKE_COMPILER_IS_GNUCC)
endif(COVERAGE)


# Text coloring option
option(USE_COLOR_OUTPUT "Use Colored output for prints!" ON)
if(USE_COLOR_OUTPUT)
    add_definitions(-DD_COLOR_OUTPUT)
endif(USE_COLOR_OUTPUT)

# Directory listing
add_subdirectory(tools)
add_subdirectory(structs)
add_subdirectory(inout)
add_subdirectory(solver)
add_subdirectory(main)
if(UNIT_TEST)
    add_subdirectory(tests)
endif(UNIT_TEST)
