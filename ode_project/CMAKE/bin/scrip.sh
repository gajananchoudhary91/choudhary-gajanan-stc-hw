#!/bin/bash
#########################################################################
# Script usage:
# 1. Enter the full path to project root in this script at line:
#    project_root_path="<project_root_path>"
#
# 2. Use one or more of the following command line arguments to
#    run the script (without the quotes):
#    a. "noclean" or "nc" to disable cleaning of build directory CMAKE/
#    b. "tailout" or "tail" or "to" to show tail output of all runs
#    c. "test" or "t" to perform unit testing using Ctest
#    d. "justclean" or "jc" to only clean the build directory and stop
#
# 3. If no command line arguments are given, then default options are:
#    a. Build directory <project_root_path>/CMAKE/ is cleaned before
#       compiling ODE
#    b. Tail outputs of simulations files are disabled
#    c. No unit testing is performed
#
# 4. Some examples for running this script:
#    ./script.sh  justclean
#    ./script.sh  to t
#
#########################################################################
# USER INPUT BEFORE USING SCRIPT:

## Normal runs
testdir="verification"                            # Test case folder name
#testfile="input"                # Test case file name (without extension)
#testfile="problem1_FEULER"      # Test case file name (without extension)
#testfile="problem1_RK2"         # Test case file name (without extension)
#testfile="problem1_RK4"         # Test case file name (without extension)
#testfile="problem1_RKF45"       # Test case file name (without extension)
#testfile="problem2_FEULER"      # Test case file name (without extension)
#testfile="problem2_RK2"         # Test case file name (without extension)
#testfile="problem2_RK4"         # Test case file name (without extension)
#testfile="problem2_RKF45"       # Test case file name (without extension)

if [ "$HOSTNAME" = "harvey.ices.utexas.edu" ]; then                # ICES
    project_root_path="/workspace/gajanan/scientific_computing/choudhary-gajanan-stc-hw/ode_project"
    TestcaseRepoPath="$project_root_path"
    compiler_module="gcc/7.2"    # This module will be loaded via module load
elif [ "$USER" = "gajanan" ]; then                             # Stampede
    project_root_path="/work/03194/gajanan/stampede2/choudhary-gajanan-stc-hw/ode_project"
    TestcaseRepoPath="$project_root_path"
    compiler_module="gcc/7.1.0"    # This module will be loaded via module load
else
    echo " "
    echo "Please open the script and supply the correct path to the project_root and test case folders."
    echo "WARNING: Use this script at your own risk. This script creates and deletes files automatically, and has NOT been tested conclusively to check for errors. Use this script IF AND ONLY IF you are absolutely sure of what you are doing!"
    echo "To help you, this script works correctly if you have given the correct path to the project_root folder and the test cases. If your project_root_path is wrong, BAD THINGS WILL HAPPEN!"
    echo " "
    exit -1
fi
coloronoff="ON"
debugonoff="OFF"
gslonoff="ON"
gprofonoff="OFF"
gcovonoff="OFF"
testonoff="OFF"
#compiler_module="intel"    # This module will be loaded via module load
####################################################################################################################

#    DO NOT EDIT THIS SCRIPT BEYOND THIS LINE!!!!!!!!!!

####################################################################################################################







####################################################################################################################
clear

if [ ! -d $project_root_path ]; then
   echo "ODE root directory path '$project_root_path' not found. Please open the script and specify the FULL path to the variable 'project_root_path' manually";
   echo "Terminating bash run"
   exit -1
fi
if [ ! -d $TestcaseRepoPath ]; then
   echo "Testcase repository '$TestcaseRepoPath' not found. Please open the script and specify the FULL path to the test case repository manually";
   echo "Terminating bash run"
   exit -1
fi

echo "";
echo "-----------------------------------------------";
echo "Test case \"$testdir\" chosen for simulation"
if [ ! -d "$TestcaseRepoPath/$testdir" ]; then
  echo "Could not find the folder \"$testdir\" at path $TestcaseRepoPath/";
  echo "Terminating bash run"
  exit -1
else
  echo "Found \"$testdir\" directory: \"$TestcaseRepoPath/$testdir\"";
fi
#sleep 0.5

cleanstatus=-1
tailoutstatus=-1
#debugstatus=-1
teststatus=-1
cmake_args=""

####################################################################################################################
checkpoint(){
if [ $1 -eq -2 ]; then
  echo;echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
  echo "Error: Conflicting command line arguments supplied by user. Terminating script."
  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
  exit -1
fi
if [ $1 -eq -3 ]; then
  echo;echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
  echo "Error: Directory $2 not found! Terminating script."
  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
  exit -1
fi
if [ $1 -ne 0 ]; then
  echo;echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
  echo "Some command was prevented from execution or proceeded incorrectly. Terminating script."
  echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
  exit -1
fi
}
shortecho() { echo ""; echo "-----------------------------------------------"; }
longecho() { echo ""; echo "-----------------------------------------------------------------------------------------------------"; }

####################################################################################################################
longecho
for arg in "$@" ;do
  shortecho
  echo "CHECKING ARGUMENT USER ARGUMENT: $arg";echo;
########################################################## For Debug
  if [ "$arg" = "debug" ] || [ "$arg" = "d" ] || [ "$arg" = "DEBUG" ] || [ "$arg" = "D" ]; then
    debugonoff="ON"
    echo "Build type DEBUG detected:"
    echo "    -- Debugging turned on"
    #sleep 0.5
  fi
########################################################## For Unit tests
  if [ "$arg" = "test" ] || [ "$arg" = "t" ] || [ "$arg" = "TEST" ] || [ "$arg" = "T" ]; then
    teststatus=1
    testonoff="ON"
    echo "Build type UNIT TESTING option detected:"
    echo "    -- Unit Testing using Ctest turned on"
    #sleep 0.5
  fi
########################################################## For Profiling
  if [ "$arg" = "profile" ] || [ "$arg" = "p" ] || [ "$arg" = "PROFILE" ] || [ "$arg" = "P" ]; then
    gprofonoff="ON"
    echo "Build type PROFILING option detected:"
    echo "    -- Profiling turned on"
    #sleep 0.5
  fi
########################################################## For Coverage
  if [ "$arg" = "coverage" ] || [ "$arg" = "c" ] || [ "$arg" = "COVERAGE" ] || [ "$arg" = "C" ]; then
    gcovonoff="ON"
    testonoff="ON"
    teststatus=1
    echo "Build type COVERAGE option detected:"
    echo "    -- Coverage turned on"
    echo "    -- Basic Debugging (-g) turned on"
    echo "    -- Unit Testing using Ctest turned on"
    #sleep 0.5
  fi
########################################################## For CLEANING BUILD
  if [ "$arg" = "justclean" ] || [ "$arg" = "jc" ]; then
    cleanstatus=2
    echo "'Just clean - don't build' option detected. All CMake files will be deleted."
    #sleep 0.5
  fi
########################################################## For CLEANING BUILD
  if [ "$arg" = "noclean" ] || [ "$arg" = "nc" ]; then
    cleanstatus=0
    echo "'Don't clean build' option detected. No files will be deleted before running CMAKE/simulation"
    #sleep 0.5
  fi
########################################################## For TAIL OUTPUT
  if [ "$arg" = "to" ] || [ "$arg" = "tail" ] || [ "$arg" = "tailout" ]; then
    tailoutstatus=1
    echo "Tail output of all simulation runs will be printed"
    #sleep 0.5
  fi
##########################################################

done
####################################################################################################################

#sleep 0.5

########################################################## For CLEANING BUILD - DEFAULT
  if [ $cleanstatus -eq -1 ]; then
    cleanstatus=1
    shortecho
    echo "Build cleaning option was not specified by user"
    echo "     - Default: Build directory /CMAKE will be cleaned"
    #sleep 0.5
  fi
########################################################## FOR NOT PRINTING TAIL OUTPUT - DEFAULT
  if [ $tailoutstatus -eq -1 ]; then
    tailoutstatus=0
    shortecho
    echo "Tail output option was not specified by user"
    echo "     - Default: Tail output of simulation files will not be printed"
    #sleep 0.5
  fi
##########################################################

longecho
####################################################################################################################

##########################################################
if [ ! -d "$project_root_path/CMAKE" ]; then  mkdir "$project_root_path/CMAKE"; fi
if [ ! -d "$project_root_path/CMAKE" ]; then  checkpoint -3 "$project_root_path/CMAKE"; fi
cd "$project_root_path/CMAKE/"

rm -r "bin/$testdir" >& /dev/null

if [ $cleanstatus -gt 0 ]; then
  shortecho
  echo "Cleaning previous build, if any..."
  echo "     - Deleting folders"
  rm -r bin/ode CMakeFiles inout lib main structs solver tools bin/gmon.out >& /dev/null
  echo "     - Deleting remaining files"
  rm Makefile CTestCustom.cmake cmake_install.cmake CMakeCache.txt  >& /dev/null
  echo "     - Cleaning complete"
fi
rm -r bin/ode bin/unit_test* tests Testing CTestTestfile.cmake  >& /dev/null
if [ $cleanstatus -eq 2 ]; then
  # Just cleaning!
  exit 0;
fi

##########################################################
shortecho
if [ "$HOSTNAME" = "harvey.ices.utexas.edu" ]; then
    module load c7
    checkpoint $?
fi
echo "Loading modules $compiler_module"
module load $compiler_module
checkpoint $?
if [ "$gslonoff" = "ON" ]; then
    module load gsl
    checkpoint $?
fi
module list
##########################################################

####################################################################################################################
longecho
cmake_args="$cmake_args -DUSE_COLOR_OUTPUT=$coloronoff -DDEBUG=$debugonoff -DUNIT_TEST=$testonoff \
                        -DUSE_GSL=$gslonoff -DPROFILING=$gprofonoff -DCOVERAGE=$gcovonoff"
cmake $cmake_args ..
checkpoint $?
shortecho
make
checkpoint $?
if [ "$testonoff" = "ON" ]; then
  shortecho
  echo ""
  echo "Performing unit testing"
  echo ""
  ctest
  checkpoint $?
  shortecho
  echo ""
  echo "Plotting convergence tests from unit_test_convergence_* unit tests."
  cd $project_root_path/CMAKE/bin/
  if [ ! -d "unit_test_output" ]; then  mkdir "unit_test_output"; fi
  cp ../tests/unit_test* unit_test_output/
  cp ../Testing/Temporary/LastTest.log unit_test_output/
  cd unit_test_output/
  module load python
  ########################################################## CONVERGENCE PLOT
  cp $project_root_path/scripts/convergence.py .
  python convergence.py;
  checkpoint $?;
  echo "Please check $project_root_path/CMAKE/bin/unit_test_output for saved plots of name *_convergence_plot.pdf"
  ########################################################## SOLUTION PLOT
  cp $project_root_path/scripts/solution.py .
  python solution.py;
  checkpoint $?;
  echo "Please check $project_root_path/CMAKE/bin/unit_test_output for saved plots of name *_solution_plot.pdf"
  ##########################################################
  # Back to CMAKE/bin/ folder.
  cd ../
fi
longecho
#sleep 1
####################################################################################################################

##########################################################
  cd "$TestcaseRepoPath"/
  echo "Copying all input files to $project_root_path/CMAKE/bin/"
  mkdir "$project_root_path/CMAKE/bin/$testdir"
  #cp "$testdir/$testfile"* "$project_root_path/CMAKE/bin/$testdir/"
  cp "$testdir/"*.dat "$project_root_path/CMAKE/bin/$testdir/"
##########################################################
 
  
  cd "$project_root_path/CMAKE/bin/"
##########################################################
  echo "Copying project executable to $project_root_path/CMAKE/bin/$testdir/"
  cp ode "$testdir/"
####################################################################################################################
  
##########################################################
  cd $testdir
  for testfilefull in *.dat; do
      testfile=${testfilefull:0:-4}
      longecho
      rm  output.$testfile $testfile-sol.dat  >& /dev/null
      echo "\"$testfile\" RUN STARTED..."
      ####################################
          ./ode $testfile > "output.$testfile"
      ####################################
      if [ $? -eq 0 ]; then
        echo "\"$testfile\" RUN COMPLETED."
        if [ $tailoutstatus -eq 1 ]; then
          shortecho
          echo "TAIL SIMULATION OUTPUT \"$testfile\"; FILE \"output.$testfile\":";
          echo ". . ."; echo ". . .";
          tail "output.$testfile"
          longecho
        fi
      else
        echo "\"$testfile\" RUN EXITED WITH ERRORS. FOLLOWING IS THE TAIL OUTPUT:";
        echo ". . ."; echo ". . .";
        tail "output.$testfile"
      fi
  done
  #rm -r ode >& /dev/null
  cd ..
 
  #echo "Creating a Tar archive of the results"; echo ". . .";
  #tar -cvf "$testdir.tar"  "$testdir/" 
####################################################################################################################

####################################################################################################################

longecho
echo ""
echo "\"$testdir/$testfile\" TEST CASE COMPLETED. PLEASE DOUBLE CHECK FOR ERRORS MANUALLY."
echo "Please check $project_root_path/CMAKE/bin/ for saved simulation files"
longecho


####################################################################################################################
####################################################################################################################
################################   FINISHED COMPILING AND RUNNING THE PROGRAM   ####################################
####################################################################################################################
####################################################################################################################

# Use python scripts to plot test case convergence results.
shortecho
echo ""
echo "Plotting solution for problems in $testdir"
cd $project_root_path/CMAKE/bin/$testdir
module load python
########################################################## SOLUTION PLOT
cp $project_root_path/scripts/problem1.py .
python problem1.py;
checkpoint $?;
########################################################## SOLUTION PLOT
cp $project_root_path/scripts/problem2.py .
python problem2.py;
checkpoint $?;
echo "Please check $project_root_path/CMAKE/bin/$testdir for saved plots of name *_solution_plot.pdf"
##########################################################
# Back to CMAKE/bin/ folder.
cd ../
