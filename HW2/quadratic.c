/* About: This program calculates the real/complex roots of a quadratic equation with real coefficients.
 * Usage: Compile this program by running:  gcc -lm -g quadratic.c  -o quadratic
 *        There are two ways to run the program: (a) run: ./quadratic <a_value> <b_value> <c_value>
 *                                               (b) run: ./quadratic <input_file>
 *        The input file should contain space/tab separated a, b, c values, with 1 set of {a, b, c} values per row.
 * Output: For a successful run, the program prints the roots for each quadratic equation on the standard output (screen).
 *         Additionally in the case when an input file is supplied in the command line arguments, an output file named
 *         roots.txt is written. It is file containing comma-separarted containing roots of the corresponding quadratic equation.
 */



#include "global_header.h"

static int DEBUG = OFF;

/****************************************************************************************************************************************************/
/****************************************************************************************************************************************************/
/****************************************************************************************************************************************************/
int main(int argc, char *argv[]){
    double a, b, c; /* Coefficients of the quadratic */
    int status;

    if (argc == 4){  /* Command line arguments expected to supply 3 coefficients a, b, c. */
        a = read_dbl_field(&(argv[1]),&status);
        b = read_dbl_field(&(argv[2]),&status);
        c = read_dbl_field(&(argv[3]),&status);

        if ((a<SMALL) && a>-SMALL){
            printf("\nWarning: the coefficient of x^2 is either 0.0 or very small.");
        }

        find_roots(a, b, c, NULL);
    }
    else if (argc == 2){ /* Command line arguments expected to supply an input file for the coefficients. Output written in <argv[1]>.out */
        char line[MAXLINE];
        char *data;      /*pointer within a line for reading double fields. */
        FILE *fpin, *fpout;
        fpin=fopen(argv[1], "r");
        fpout=fopen("roots_updated.txt", "w");

        while(fgets(line,MAXLINE,fpin)){
            data=line;
            a=read_dbl_field(&data,&status);
            b=read_dbl_field(&data,&status);
            c=read_dbl_field(&data,&status);
            find_roots(a, b, c, fpout);
        }
    }
    else {
        throw_error("Error: Need the coefficients \'a, b, c\' as command line arguments, either as numbers, or as an input file. Exiting.");
    }

    printf("\nSuccessfully exiting program.\n\n\n");
    return 0;
}
/****************************************************************************************************************************************************/
/****************************************************************************************************************************************************/
/****************************************************************************************************************************************************/
void find_roots(double a, double b, double c, FILE *fpout) {
    double x_1, x_2, R_x, C_x; /* solution to the quadratic */
    double discriminant = b*b-4*a*c;
    if (discriminant < 0.0){
        //throw_error("Error: The discriminant (b*b - 4*a*c) < 0.0. The roots of the quadratic equation are complex. Exiting.");
        R_x = -0.5*b/a;
        C_x = 0.5*sqrt(-discriminant)/a;
        printf("\n    x_1 = % 23.16e + % 23.16e*i\n    x_2 = % 23.16e - % 23.16e*i", R_x, C_x, R_x, C_x);
        if (fpout!=NULL) fprintf(fpout,"% 23.16e + % 23.16e*i,    % 23.16e + % 23.16e*i\n", R_x, C_x, R_x, C_x);
    }
    else{
        x_1 = 0.5*(-b + sqrt(discriminant))/a;
        x_2 = 0.5*(-b - sqrt(discriminant))/a;
#ifdef _DEBUG
        if (DEBUG){
            printf("\nNaive way of calculating roots: x_1 = % 23.16e, x_2 = % 23.16e", x_1, x_2);
            printf("\n    a(x_1)^2 + b(x_1) + c = % 23.16e", a*x_1*x_1 + b*x_1 + c);
            printf("\n    a(x_2)^2 + b(x_2) + c = % 23.16e", a*x_2*x_2 + b*x_2 + c);
        }
#endif

        if (fabs(x_1) > fabs(x_2)) {
            x_2=2*c/(-b + sqrt(discriminant));
        }
        else if (fabs(x_2) > fabs(x_1)) {
            x_1=2*c/(-b - sqrt(discriminant));
        }
#ifdef _DEBUG
        if (DEBUG){
            printf("\n\nBetter way of calculating roots: x_1 = % 23.16e, x_2 = % 23.16e", x_1, x_2);
            printf("\n    a(x_1)^2 + b(x_1) + c = % 23.16e", a*x_1*x_1 + b*x_1 + c);
            printf("\n    a(x_2)^2 + b(x_2) + c = % 23.16e", a*x_2*x_2 + b*x_2 + c);
        }
#endif

        printf("\n\nThe roots of the equation % 23.16ex^2 + % 23.16ex + % 23.16e = 0 are:", a, b, c);
        printf("\n    x_1 = % 23.16e\n    x_2 = % 23.16e", x_1, x_2);

        if (fpout!=NULL) fprintf(fpout,"% 23.16e,    % 23.16e\n", x_1, x_2);
    }

}
/****************************************************************************************************************************************************/
/****************************************************************************************************************************************************/
/****************************************************************************************************************************************************/
double read_dbl_field(char **outstr, int *status){
    double outdouble = NO;
    int pcount = 0, ecount=0;
    int numlength=0;
    char *i = *outstr;
    remove_spaces(&i);
    *outstr = i;
    if (*i=='-' || *i=='+') {
        i++;
    }
    if ((!*i) || *i==0 || *i=='\n' || *i==' ' || *i == '\t'){
#ifdef _DEBUG
        if (DEBUG) printf("\nError: No number encountered in the string");
        else throw_error("\nError: No number encountered in the string");
#else
        throw_error("\nError: No number encountered in the string");
#endif
        *status = READ_FAILURE;
        return NO;
    }
    while (*i!=' ' && *i!='\t' && *i!='\n' && *i!=0){
        if (*i>='0' && *i<='9'){
            numlength++;
        }
        else if (pcount<1 && *i=='.' && numlength>=0){
            pcount++;
        }
        else if (ecount<1 && (*i=='e' || *i=='E') && numlength>0){
            ecount++;
        }
        else if (((*(i-1))=='E' || (*(i-1))=='e') && (*i=='-' || *i=='+')){
            /* Do nothing, this one's allowed. */
        }
        else{
#ifdef _DEBUG
            printf("\n%s", i);
            if(DEBUG) printf("\nError: Encountered a non-numeric character while reading the double.");
            else throw_error("\nError: Encountered a non-numeric character while reading the double.");
#else
            throw_error("\nError: Encountered a non-numeric character while reading the double.");
#endif
            *outstr = i;
            *status = READ_FAILURE;
            return NO;
        }
        i++;
    }
    if (numlength ==0){
#ifdef _DEBUG
        if (DEBUG) printf("\nError: No number encountered in the string");
        else throw_error("\nError: No number encountered in the string");
#else
        throw_error("\nError: No number encountered in the string");
#endif
        *status = READ_FAILURE;
        return NO;
    }

    /* If we're here, we can finally read the string! */
    outdouble = (double) strtod(*outstr, &i);
#ifdef _DEBUG
    if (DEBUG) printf("\n outstr : %s      i : %s", *outstr, i);
#endif
    *outstr = i;
    *status = READ_SUCCESS;
    return outdouble;
}
/****************************************************************************************************************************************************/
/****************************************************************************************************************************************************/
/****************************************************************************************************************************************************/
void remove_spaces(char **outstr){
    char *i = *outstr;
    while ((*i == ' ' || *i == '\t') && (*i!=0) && (*i!='\n')){
        i++;
    }
    if (*i!=0 /* && *i!='\n'*/) {
        *outstr = i;
    }
    else{
        *outstr = NULL;
    }
}
/****************************************************************************************************************************************************/
/****************************************************************************************************************************************************/
/****************************************************************************************************************************************************/
