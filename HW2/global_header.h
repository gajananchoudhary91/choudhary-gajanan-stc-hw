#ifndef _DEFINE_H
#define _DEFINE_H

#include <stdio.h>
#include <stdlib.h>
//#include <stddef.h>
#include <assert.h>
#include <string.h>
//#include <ctype.h>
#include <math.h>
//#include <float.h>
//#include <limits.h>
//#include <time.h>

/* Constants */
#define SMALL  1.0E-06
#define ON     1
#define OFF    0
#define YES    1
#define NO    -3
#define TRUE   1
#define FALSE  0

/* I/O */
#define MAXLINE 250
#define MAX_INT_LENGTH 9
#define READ_SUCCESS   1
#define READ_FAILURE   0

/* Macros */
#define throw_error(s) {printf("\n\n"); printf(s); printf("\n\n"); exit(-1);}

/* Function declarations  */
void find_roots(double, double, double, FILE *);
void remove_spaces(char **);
double read_dbl_field(char **, int *);

#endif
